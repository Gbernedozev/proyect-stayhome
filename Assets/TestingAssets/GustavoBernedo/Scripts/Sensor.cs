﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : Door
{
    public bool isSomeone;
    public Renderer lightDoor;
    public Material lightOn;
    public Material lightOff;

    public enum TypeEntry
    {
        Player,
        GroundBot,
        Airbot
    }

    public TypeEntry type;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            lightDoor.material = lightOn; 
        }
        else
        {
            lightDoor.material = lightOff;
        }



    }



    /*
    private void OnTriggerEnter(Collider other)
    {
        if (isActive && other.tag == type.ToString())
        {
            isSomeone = true;
            anim.SetBool("isSomeone",isSomeone);
            GetComponentInParent<AudioClipPlayer>().PlayAudio(0,1.2f);
        }
    }*/

    private void OnTriggerStay(Collider other)
    {
        if (isActive && other.tag == type.ToString())
        {
            if (!isSomeone)
            {
                GetComponentInParent<AudioClipPlayer>().PlayAudio(0, 1.2f);
            }
            isSomeone = true;
            anim.SetBool("isSomeone", isSomeone);
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isActive && other.tag == type.ToString())
        {
            isSomeone = false;
            anim.SetBool("isSomeone", isSomeone);
            GetComponentInParent<AudioClipPlayer>().PlayAudio(0,1.2f);
        }
    }

    public void OpenDoor()
    {
        isActive = true;
    }

    public void CloseDoor()
    {
        isActive = false;
    }
}
