﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialActivator : MonoBehaviour
{
    public int indice;
    TutorialControler tutorial;
    bool isfirstTime=true;
    // Start is called before the first frame update
    void Start()
    {
        tutorial = GameObject.FindGameObjectWithTag("Tutorial").GetComponent<TutorialControler>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Player" && isfirstTime)
        {
            tutorial.ActivateTutorial(indice);
            isfirstTime = false;
        }
    }
}
