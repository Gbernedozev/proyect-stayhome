﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerHUD : MonoBehaviour
{


     WeinerController player;
    [Header("Elementos del Hud")]
    [HideInInspector]public int numCellsIcons=5;
    public Image[] allCelts;
    [HideInInspector] public int numNodesIcons=5;
    public Image[] allNodes;
    public Image[] icons;

    CharManager manager;
    //Var for udpating hp
    int hpcomparer;
    //Var for updating character selected:
    string lastCharacter;
    //Var for updating nodes
    int nodecomparer;



    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("CharControl").GetComponent<CharManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<WeinerController>();
        hpcomparer = player.currentHP;
        nodecomparer = player.currentNodos;
        lastCharacter = manager.charSelected;
        HpUpdater();
        DronSelectorUpdater();
        NodeUpdater();
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.actualCharacter == "Weiner")
        {
            //UpdatingHP:
            if (hpcomparer != player.currentHP)
            {
                HpUpdater();
                hpcomparer = player.currentHP;
            }

            //Updating Dron selector

            if (lastCharacter != manager.charSelected)
            {

                DronSelectorUpdater();
                lastCharacter = manager.charSelected;
            }

            //Updating Nodes

            if (nodecomparer != player.currentNodos)
            {
                NodeUpdater();
                nodecomparer = player.currentNodos;
            }
        }
        
    }


    public void HpUpdater()
    {
        for (int i = 0; i < allCelts.Length; i++)
        {

            if (i < player.currentHP)
            {
                Image imagen = allCelts[i].GetComponent<Image>();
                imagen.CrossFadeColor(Color.green, 1f, true, true);

            }
            else
            {
                Image imagen = allCelts[i].GetComponent<Image>();
                imagen.CrossFadeColor(Color.red, 1f, true, true);
            }

            if (i < numCellsIcons)
            {
                allCelts[i].enabled = true;
            }
            else
            {
                allCelts[i].enabled = false;
            }
        }
    }

    public void DronSelectorUpdater()
    {
        if (manager!=null)
        {
            if (manager.charSelected == "Groundbot")
            {
                Image imagen = icons[0].GetComponent<Image>();
                imagen.CrossFadeColor(Color.yellow, 0.4f, true, true);

                Image imagen2 = icons[1].GetComponent<Image>();
                imagen2.CrossFadeColor(Color.white, 0.4f, true, true);

            }


            if (manager.charSelected == "Airbot")
            {
                Image imagen = icons[0].GetComponent<Image>();
                imagen.CrossFadeColor(Color.white, 0.4f, true, true);

                Image imagen2 = icons[1].GetComponent<Image>();
                imagen2.CrossFadeColor(Color.yellow, 0.4f, true, true);
            }

        }
        else
        {
            Image imagen = icons[0].GetComponent<Image>();
            imagen.CrossFadeAlpha(50,0.1f,false);

            Image imagen2 = icons[1].GetComponent<Image>();
            imagen2.CrossFadeAlpha(50, 0.1f, false);

        }

    }

    public void NodeUpdater()
    {
        
        for (int i = 0; i < allNodes.Length; i++)
        {

            if (i < player.currentNodos)
            {
                Image imagen = allNodes[i].GetComponent<Image>();
                Color color = imagen.color;
                imagen.CrossFadeColor(Color.red, 0.1f, true, false);
                

            }
            else
            {
                
                Image imagen = allNodes[i].GetComponent<Image>();
                Color color = imagen.color;
                imagen.CrossFadeColor(Color.white, 0.1f, true, false);
                
                
            }

            if (i < numNodesIcons)
            {
                allNodes[i].enabled = true;
            }
            else
            {
                allNodes[i].enabled = false;
            }
        }
    }

}
