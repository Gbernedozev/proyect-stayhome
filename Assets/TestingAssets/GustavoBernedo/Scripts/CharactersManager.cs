﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class CharactersManager : MonoBehaviour
{
    //Preferencia en uso de la camara:

    string actualCharacter;

    //Referencias de la camara de Drones

    public CinemachineFreeLook cinemachine;
    public GameObject cinemachineObject;
    public GameObject dronCamera;

    ///Referencias del jugador
    public CharacterController playermotor;
    public CharacterView player;
    public Transform locationReference;
    public GameObject camaraPlayer;

    //Referencias Groundbot:

    public GameObject groundbotSkin;
    public Transform groundbotPos;
    public CharacterController groundbotmotor;
    public B0ots groundbot;

    //Referencias Airbot:
    
    public GameObject airbotSkin;
    public Transform airbotPos;
    public CharacterController airbotmotor;
    public Map airbot;

    // Variable de seleccion:
    int botOption;

   


    // Start is called before the first frame update
    void Start()
    {
        actualCharacter = "Weiner";
        groundbot.isDeployed = false;
        groundbot.isActive = false;
        airbot.isDeployed = false;
        player.isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        
        switch (actualCharacter)
        {
            case "Weiner":
                SelectionBot();
                ActivateBot();
                SwitchCharactertoDron();
                break;
            case "Boots":
                SwitchBottoCharacter();
                break;
            case "Map":
                SwitchBottoCharacter();
                break;
        }
        
        

    }

    void SelectionBot()
    {
        
            if (Input.GetKeyDown(KeyCode.Q))
            {
                botOption = 0;
                Debug.Log("Seleccionaste Groundbot");

            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                botOption = 1;
                Debug.Log("Seleccionaste Airbot");
            }
        
    }

    void ActivateBot()
    {
        switch (botOption)
        {
            case 0:
                if (Input.GetKeyDown(KeyCode.R) && player.isActive)
                {
                    
                    ActivateGroundBot();
                }
                break;
            case 1:
                if (Input.GetKeyDown(KeyCode.R) && player.isActive)
                {
                    ActivateAirBot();
                }
                break;
        }
    }

    /*
    void DeActivate()
    {
        
    }
    */

    public void ActivateGroundBot()
    {
        if (groundbot.isDeployed)
        {

            return;
        }
        else
        {
            
            Vector3 pos = locationReference.transform.position;
            player.isActive = false;
            player.currentHealth -= 1;
            playermotor.enabled = false;
            camaraPlayer.SetActive(false);

            actualCharacter = "Boots";
            groundbotPos.transform.position = pos;
            groundbot.enabled = true;
            groundbotmotor.enabled = true;
            groundbotSkin.SetActive(true);
            groundbot.isDeployed = true;
            groundbot.isActive = true;
            groundbot.health = 1;


            dronCamera.SetActive(true);
            cinemachineObject.SetActive(true);
            cinemachine.LookAt = groundbotPos.transform;
            cinemachine.Follow = groundbot.transform;
            
        }

    }

    public void ActivateAirBot()
    {
        if (airbot.isDeployed)
        {

            return;
        }
        else
        {
            Vector3 pos = locationReference.transform.position;
            player.isActive = false;
            player.currentHealth -= 1;
            playermotor.enabled = false;
            camaraPlayer.SetActive(false);

            actualCharacter = "Map";
            airbotPos.transform.position = pos;
            airbot.enabled = true;
            airbotmotor.enabled = true;
            airbotSkin.SetActive(true);
            airbot.isDeployed = true;
            airbot.isActive = true;
            airbot.health = 1;


            dronCamera.SetActive(true);
            cinemachineObject.SetActive(true);
            cinemachine.LookAt = airbotPos.transform;
            cinemachine.Follow = airbot.transform;


        }

    }

    public void SwitchCharactertoDron()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (groundbot.isDeployed && botOption == 0)
            {
                player.isActive = false;
                playermotor.enabled = false;
                camaraPlayer.SetActive(false);
                actualCharacter = "Boots";
                groundbot.isActive = true;
                dronCamera.SetActive(true);
                cinemachineObject.SetActive(true);
                cinemachine.LookAt = groundbotPos.transform;
                cinemachine.Follow = groundbot.transform;

            }
            if (airbot.isDeployed && botOption == 1)
            {
                player.isActive = false;
                playermotor.enabled = false;
                camaraPlayer.SetActive(false);
                actualCharacter = "Map";
                airbot.isActive = true;
                dronCamera.SetActive(true);
                cinemachineObject.SetActive(true);
                cinemachine.LookAt = airbotPos.transform;
                cinemachine.Follow = airbot.transform;
            }
        }
      
    }

    public void SwitchBottoCharacter()
    {
        switch (actualCharacter)
        {
            case "Boots":
                if (Input.GetKeyDown(KeyCode.R))
                {
                    
                    groundbot.isActive = false;
                    dronCamera.SetActive(false);
                    cinemachineObject.SetActive(false);

                    playermotor.enabled = true;
                    player.isActive = true;
                    camaraPlayer.SetActive(true);
                    actualCharacter = "Weiner";
                }
                
                break;

                case "Map":
                if (Input.GetKeyDown(KeyCode.R))
                {
                    
                    airbot.isActive = false;
                    dronCamera.SetActive(false);
                    cinemachineObject.SetActive(false);

                    playermotor.enabled = true;
                    player.isActive = true;
                    camaraPlayer.SetActive(true);
                    actualCharacter = "Weiner";
                }
                
                break;
            }
    }
}
