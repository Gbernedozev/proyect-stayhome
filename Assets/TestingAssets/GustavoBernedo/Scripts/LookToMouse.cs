﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToMouse : MonoBehaviour
{
    Transform cameraTransform;
    Transform playerTransform;

    [SerializeField] float sensitivityX = 150f;
    [SerializeField] float sensitivityY = 150f;

    [SerializeField] float limitRotationXmin = -90f;
    [SerializeField] float limitRotationXmax = 90f;

    float rotateX;

    // Start is called before the first frame update
    void Start()
    {
        cameraTransform = Camera.main.transform;
        playerTransform = this.transform;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerCameraMove();
    }

    void PlayerCameraMove()
    {
        float mouseX = Input.GetAxis("Mouse X") * sensitivityX * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivityY * Time.deltaTime;

        rotateX += mouseY;
        rotateX = Mathf.Clamp(rotateX, limitRotationXmin, limitRotationXmax);

        cameraTransform.localRotation = Quaternion.Euler(-rotateX, 0, 0);
        playerTransform.Rotate(Vector3.up * mouseX);
    }
}
