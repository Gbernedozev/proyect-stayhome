﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudController : MonoBehaviour
{
    public static HudController hud;


    [Header("Todos los iconos HUD's")]
    public  GameObject playerHudIcons;
    public GameObject bootsHudIcons;
    public GameObject mapHudIcons;
    public GameObject advertView;
    public GameObject warningView;
    public TextMeshProUGUI warningText;

    [Header("Hud's Codigos de referencia")]
    public PlayerHUD playerhud;
    public MapHud maphud;
    public BootsHud bootshud;
    

    CharManager manager;
    string lastCharacter;

    private void Awake()
    {
        hud = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("CharControl").GetComponent<CharManager>();
        
        lastCharacter = manager.actualCharacter;
    }

    // Update is called once per frame
    void Update()
    {
        if (lastCharacter != manager.actualCharacter)
        {
            HUDselectedUpdater();
            lastCharacter = manager.actualCharacter;
        }
        
    }

    void HUDselectedUpdater()
    {
        if (manager.actualCharacter == "Weiner")
        {
            playerHudIcons.SetActive(true);
            bootsHudIcons.SetActive(false);
            mapHudIcons.SetActive(false);

            playerhud.HpUpdater();
            playerhud.DronSelectorUpdater();
            playerhud.NodeUpdater();


        }
        if (manager.actualCharacter == "Groundbot")
        {
            playerHudIcons.SetActive(false);
            bootsHudIcons.SetActive(true);
            mapHudIcons.SetActive(false);

            bootshud.CharSelectorUpdater();

        }
        if (manager.actualCharacter == "Airbot")
        {
            playerHudIcons.SetActive(false);
            bootsHudIcons.SetActive(false);
            mapHudIcons.SetActive(true);

            maphud.CharSelectorUpdater();
        }
    }

    public void RevealAdvert()
    {
        advertView.SetActive(true);
    }

    public void HideAdvert()
    {
        advertView.SetActive(false);
    }

    public void ShowWarning(string warning)
    {
        StartCoroutine(Danger(warning));
    }

    IEnumerator Danger(string text)
    {
        warningView.SetActive(true);
        warningText.text = text;
        yield return new WaitForSeconds(5f);
        warningView.SetActive(false);
    }

}
