﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeinerController : MonoBehaviour
{
    public static WeinerController player;

    [Header("Velocidades de Movimientos")]
    [SerializeField] float velocityForward = 10f; // velocidad defrente
    [SerializeField] float lateralVelocity = 10f; //velocidad lateral
    [SerializeField] float backVelocity = 10f; //velocidad al retroceder

    [Header("Velocidad al correr")]
    [SerializeField] float multiplicatorRunning = 1.5f; //multiplica la velocidad para correr 
    [SerializeField] bool lateralRunning = false;
    [SerializeField] bool backRunning = false;

    [Header("Salto y caidas")]
    [SerializeField] float gravity = 9.8f; //valor de la gravedad base , esto se puede modificar 
    [SerializeField]float fallingVelocity; // acumulador de la velocidad de caida
    [SerializeField] float groundedGravity = 2.5f; //velocidad de caida inicial
    [SerializeField] float jumpHight=2.5f;

    [Header("Recursos")]
    public int hp;
    [HideInInspector] public int currentHP;
    //int nodos;
    [HideInInspector] public int currentNodos;

    //Estados del personaje
    bool isFalling;
    bool isCrounching;
    bool isInteracting;
    bool isStunning;
    bool isInvulnerable;
  //  public bool isHacking;  /// Fernando Jala esta variable
    float initialFallingHight = 0;

    //Inputs del personaje
    bool inputRunning;
    bool inputJumping;
    bool inputCrounching;
    bool inputInteracting;

    CharacterController characterController;
    float playerHeight;
    ///Char Manager:
    CharManager manager;

    // Todo conserniente con el raycast
    float castrange=2;
    public LayerMask mask;

    // CamShake:
    CamShaker camshaker;

    //audioclipplayer
    AudioClipPlayer audio_player;

    private void Awake()
    {
        player = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHP = DataKeeper.healKeerper;
        currentNodos = DataKeeper.nodesKeeper;
        camshaker = Camera.main.GetComponent<CamShaker>();
        characterController = GetComponent<CharacterController>();
        playerHeight = characterController.height;
        manager = GameObject.FindGameObjectWithTag("CharControl").GetComponent<CharManager>();
        audio_player = GetComponent<AudioClipPlayer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isStunning)
        {
            PlayerMovement();
            PlayerInteraction();
        }


        if (currentHP <= 0)
        {
            GameStatusChanger.gameStatus.DeathScreen();
            GameStatusChanger.gameStatus.StateChange();
        }

    }

    //Metodo de movimiento del personaje
    void PlayerMovement()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        inputRunning=Input.GetButton("Fire3"); // Fire3 es shift izquierdo
        //inputJumping = Input.GetButtonDown("Jump");
        inputCrounching = Input.GetButton("Fire1"); //Fire1 es control izquierdo

        Vector2 inputVector = new Vector2(x, z);

        if (inputVector.magnitude > 1)
        {
            inputVector = inputVector.normalized; 
        }

        float movementVelocityZ = CalculateVelocityZ(inputVector.y);
        float movementVelocityX = CalculateVelocityX(inputVector.x);

        Vector3 moveDirection = (transform.forward * movementVelocityZ) +(transform.right*movementVelocityX);

        //AUDIO walk run
        if (inputVector.magnitude > 0.0001f)
        {
            switch (Input.GetButton("Fire3"))
            {
                case true:
                    CallAudioPlayer(9, -1f);
                    break;
                case false:
                    CallAudioPlayer(8, -1f);
                    break;

                default:
                    CallAudioPlayer(8, -1f);
                    break;
            }
        }
        else
        {
            audio_player.StopAudio(8);
            audio_player.StopAudio(9);
        }

        if (characterController.isGrounded) // se usa una funcion del character controller para poder detectar si hay colliders abajo del personaje
        {
            if (isFalling)
            {
                float fallingDistance = initialFallingHight - transform.position.y;
               // HittingGround(fallingVelocity,fallingDistance);
                isFalling = false;
            }

            if (inputCrounching)
            {
                if (!isCrounching)                 // se agregan los estados para no sobre escribir playerheiht en cada frame
                { 
                    characterController.height = playerHeight / 2;
                    isCrounching = true;                     
                }
            }
            else
            {
                if (isCrounching)
                {
                    characterController.height = playerHeight;
                    isCrounching = false;
                }
             
            }

            if (inputJumping)
            {
                fallingVelocity=-CalculateJumpVelocity(jumpHight); // se le pone un negativo para que en moveDirection se vuelva positivo y valla para arriba
            }
            else
            {
                fallingVelocity = groundedGravity;
            }
            
        }
        else
        {
            fallingVelocity += gravity * Time.deltaTime;
            if (!isFalling&&fallingVelocity>0.001) // se debe evaluar la velocidad positiva debido a que en moveDirection esta se vuelve negativa
            {
                isFalling = true;
                initialFallingHight = transform.position.y;
            }
        }

        moveDirection += Vector3.up * -fallingVelocity * Time.deltaTime; // se vuelve a multiplicar por el tiempo debido a que hablamos de una aceleracion
        characterController.Move(moveDirection);

        //print(characterController.velocity.magnitude); //medimos la magnitud de la velocidad
        
    }

    //Metodo de Interaccion del personaje

    void PlayerInteraction()
    {
        inputInteracting = Input.GetKeyDown(KeyCode.F);

        if (inputInteracting)
        {
            RaycastHit hit;

            if (!isInteracting)
            {
                isInteracting = true;
            }

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,out hit,castrange,mask))
            {
             //   Debug.Log(hit.collider.gameObject);
                int layer = hit.collider.gameObject.layer;
                IObjectInteractable objectInteractable = hit.collider.gameObject.GetComponent<IObjectInteractable>();

                if (objectInteractable.Interactable)
                {
                    switch (layer)
                    {
                        case 10:
                     //       print("Hackeando consola"); // OK
                            GetComponent<AudioClipPlayer>().PlayAudio(0,1.2f);//play hacking noise
                            break;
                        case 11:
                       //     print("Recargar Vida"); // OK
                            // GetComponent<AudioClipPlayer>().PlayAudio(1);
                            break;
                        case 12:
                       //     print("Activar Circuito");
                           // GetComponent<AudioClipPlayer>().PlayAudio(2);
                            break;
                        case 13:
                       //     print("Interactuar con nodo del circuito");
                           // GetComponent<AudioClipPlayer>().PlayAudio(3);
                            break;
                        case 14:
                       //     print("Interactuar con nodo final del circuito");
                            // GetComponent<AudioClipPlayer>().PlayAudio(4);
                            break;
                        case 15:
                       //     print("Interactuar con boton de elevador"); // OK
                             GetComponent<AudioClipPlayer>().PlayAudio(5,1.1f);
                            break;

                        case 18:
                       //     print("repliega dron aereo");
                            manager.ReplegarMap();
                            // GetComponent<AudioClipPlayer>().PlayAudio(6);
                            break;
                        case 19:
                      //      print("repliega dron terrestre");
                            manager.ReplegarBoots();
                            // GetComponent<AudioClipPlayer>().PlayAudio(7);
                            break;
                    }
                    objectInteractable.TriggerInteraction();
                }

                     
            }
        }
        else
        {
            if (isInteracting)
            {
                isInteracting = false;
            }
        }

    }

    // Metodos de calculo de velocidades
    private float CalculateVelocityX(float inputX)
    {
        float velocityX = inputX * lateralVelocity * Time.deltaTime;

        if(lateralRunning && inputRunning)
        {
            velocityX *= multiplicatorRunning;
        }
        return velocityX;
    }

    private float CalculateVelocityZ(float inputZ)
    {
        float velocityZ;

        if (inputZ > 0)
        {
            velocityZ = velocityForward;
            if (inputRunning)
            {
                velocityZ *= multiplicatorRunning;
            }
        }
        else
        {
            velocityZ = backVelocity;
            if (backRunning && inputRunning)
            {
                velocityZ *= multiplicatorRunning;
            }
        }
        return velocityZ*Time.deltaTime*inputZ;
    }

    private float CalculateJumpVelocity(float WantedHight)
    {
        return Mathf.Sqrt(2*gravity*WantedHight);
    }

    public void HittingGround(float finalvelocity,float distance)
    {
        print("Caida de " + distance + " metros y a una velocidad final de " + finalvelocity + " m/s");
    }

    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Camera.main.transform.position, Camera.main.transform.forward*castrange);
    }
    */
    IEnumerator Invulnerable(float time)
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(time);
        isInvulnerable = false;
    }

    IEnumerator Stunning(float time)
    {
        isInvulnerable = true;
        isStunning = true;
        yield return new WaitForSeconds(time);
        isStunning = false;
        isInvulnerable = false;

    }

    public void DamageInflicted(int damage,bool stunning)
    {
        currentHP -= damage;
        StartCoroutine(camshaker.Shake(0.15f, 0.4f));
        if (stunning)
        {
            StartCoroutine(Stunning(2));
        }
    }

    //Deteccion de algun ataque enemigo:

    private void OnCollisionEnter(Collision hit)
    {
        /*
        if (hit.gameObject.tag == "bullet")
        {
            
            StartCoroutine(camshaker.Shake(0.15f, 0.4f));
            if (!isInvulnerable && currentHP > 0)
            {
                currentHP -= 1;
                //StartCoroutine(Invulnerable(1));
                Destroy(hit.gameObject);
            }


        }
        if (hit.gameObject.tag == "Kamikaze")
        {

            StartCoroutine(camshaker.Shake(0.15f, 0.4f));
            if (!isInvulnerable && currentHP > 0)
            {
                currentHP -= 2;
                StartCoroutine(Invulnerable(1));
            }

        }
        if (hit.gameObject.tag == "ForceField")
        {

            StartCoroutine(camshaker.Shake(0.15f, 0.4f));
            if (!isInvulnerable && currentHP > 0)
            {
                currentHP -= 2;
                StartCoroutine(Stunning(2));
            }


        }
        */
        //    GameObject.FindGameObjectWithTag("GlobalUI").GetComponent<GameStatusChanger>().PauseAndChangeToDeathUI();


      

    }

    void CallAudioPlayer(int audio_index, float countdown)
    {
        if (audio_index != audio_player.current_or_last_audio_index)
        {
            audio_player.PlayAudio(audio_index, countdown);
        }
        
        
    }
}
