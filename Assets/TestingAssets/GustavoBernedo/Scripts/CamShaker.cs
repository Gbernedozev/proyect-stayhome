﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CamShaker : MonoBehaviour
{
    Vector3 originalLocalPos;

    private void Start()
    {
        originalLocalPos = transform.localPosition;
    }
    public IEnumerator Shake(float duration, float magnitud)
    {
        
        float elapse=0.0f;

        while (elapse<duration)
        {
            float x = Random.Range(-1f, 1f) * magnitud;
            float y = Random.Range(-1, 1f) * magnitud;
            transform.localPosition = new Vector3(originalLocalPos.x+x,originalLocalPos.y+y, originalLocalPos.z);
            elapse += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = originalLocalPos;
    }
   
}
