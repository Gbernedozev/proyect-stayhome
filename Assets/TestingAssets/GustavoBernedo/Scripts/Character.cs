﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public CharacterController motor;
    public float speed;
    public float gravity = -9.81f;
    public float x;
    public float z;

    // Recursos del personaje
    public int currentHealth;
    public int maxhealth = 5;

    Vector3 velocity; // Velocidad de caida

    //Detecta piso
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;

    //Estado del personaje

    public bool isActive;


    //Control de vida con hud

    public PlayerHUD hud;

  //  public GameStatusChanger gameManager;

    public void RegularMovement()
    {
        if (isActive)
        {
            x = Input.GetAxis("Horizontal");
            z = Input.GetAxis("Vertical");

            Vector3 move = transform.right * x + transform.forward * z;

            motor.Move(move * speed * Time.deltaTime);

            velocity.y += gravity * Time.deltaTime;

            motor.Move(velocity * Time.deltaTime);
        }

    }

    public void CheckSphere()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
    }

    public void RestoreFullHealth()
    {
        currentHealth = maxhealth;
    }

    public void ReduceHealth(int damage)
    {
        currentHealth -= damage;
        //    hud.HudUpdater();

        if (currentHealth <= 0)
        {
            GameStatusChanger.gameStatus.DeathScreen();
            GameStatusChanger.gameStatus.StateChange();
        //    gameManager.PauseAndChangeToDeathUI();
        }
    }

}
