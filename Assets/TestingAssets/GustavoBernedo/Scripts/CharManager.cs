﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CharManager : MonoBehaviour
{
    [HideInInspector]public string actualCharacter;
    [HideInInspector] public string charSelected;
    [Header("Objetos")]
    [SerializeField] GameObject weiner;
    [SerializeField] GameObject boots;
    [SerializeField] GameObject map;

    //Referencias de Weiner
    WeinerController weinerRef;
    CharacterController weinerMotor;
    LookToMouse weinercamControl;
    //Referencias de Boots
    B0ots bootsRef;
    CharacterController bootsMotor;
    Vector3 bootsSavePos;
    //Vector3 bootsSaveRot;
    //Referencias de Map
    Map mapRef;
    CharacterController mapMotor;
    Vector3 mapSavePos;
    //Vector3 mapSaveRot;

    [Header("Camaras Referencias")]
    public GameObject weinerCam;
    public  GameObject dronesCam;
    public GameObject cinemachineobj;

    CinemachineFreeLook machineRef;

    //Variable de control si map esta anclado a bot 
    bool isMapwithBoots;
    
    // Start is called before the first frame update
    void Start()
    {
        isMapwithBoots = true;

        //Weiner:
        weiner = GameObject.FindGameObjectWithTag("Player");
        weinerRef = weiner.GetComponent<WeinerController>();
        weinerMotor = weiner.GetComponent<CharacterController>();
        weinercamControl = weiner.GetComponent<LookToMouse>();

        //Boots:
        boots = GameObject.FindGameObjectWithTag("GroundBot");
        bootsRef = boots.GetComponent<B0ots>();
        bootsMotor = boots.GetComponent<CharacterController>();
        bootsSavePos = boots.transform.localPosition;
        
        //Map:
        map = GameObject.FindGameObjectWithTag("Airbot");
        mapRef = map.GetComponent<Map>();
        mapMotor = map.GetComponent<CharacterController>();
        mapSavePos = map.transform.localPosition;
        //get cinemachine and select var:
        machineRef = cinemachineobj.GetComponent<CinemachineFreeLook>();
        actualCharacter = "Weiner";
        charSelected = "Groundbot";
    }

    // Update is called once per frame
    void Update()
    {
        SelectCharacter();
        ActivateCharacter(charSelected);
    }

    void SelectCharacter()
    {
        switch (actualCharacter)
        {
            case "Weiner":
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    charSelected = "Groundbot";
                    //Debug.Log("Seleccionaste Groundbot");

                }
                else if (Input.GetKeyDown(KeyCode.E))
                {
                    charSelected = "Airbot";
                    //Debug.Log("Seleccionaste Airbot");
                }

                break;
            case "Groundbot":
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    charSelected = "Weiner";
                    //Debug.Log("Seleccionaste a Weiner");

                }
                else if (Input.GetKeyDown(KeyCode.E))
                {
                    charSelected = "Airbot";
                    //Debug.Log("Seleccionaste Airbot");
                }
                break;
            case "Airbot":
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    charSelected = "Weiner";
                   // Debug.Log("Seleccionaste a Weiner");

                }
                else if (Input.GetKeyDown(KeyCode.E))
                {
                    charSelected = "Groundbot";
                   // Debug.Log("Seleccionaste Groundbot");
                }
                break;
        }
    }

    void ActivateCharacter(string character)
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (actualCharacter == "Weiner")
            {
                switch (character)
                {
                    case "Groundbot":
                        ActivateGroundbot();
                        break;
                    case "Airbot":
                        ActivateAirbot();
                        break;
                }
            }
            if(actualCharacter == "Groundbot")
            {
                switch (character)
                {
                    case "Weiner":
                        ActivateWeiner();
                        break;
                    case "Airbot":
                        ActivateAirbot();
                        break;
                }
            }
            if (actualCharacter == "Airbot")
            {
                switch (character)
                {
                    case "Weiner":
                        ActivateWeiner();
                        break;
                    case "Groundbot":
                        ActivateGroundbot();
                        break;
                }
            }
        }
    }
    
    void ActivateGroundbot()
    {
        Vector3 spawnPosition = weiner.transform.position;

        if (bootsRef.isDeployed)
        {
            if (actualCharacter == "Weiner")
            {
                
                ///Desactivas Weiner
                weinerRef.enabled = false;
                weinerCam.SetActive(false);
                weinercamControl.enabled = false;
                //Activas Camaras
                dronesCam.SetActive(true);
                cinemachineobj.SetActive(true);
                machineRef.LookAt = boots.transform;
                machineRef.Follow = boots.transform;
                ///Activas el Dron
                bootsRef.isActive = true;
                actualCharacter = charSelected;
            }
            if (actualCharacter == "Airbot")
            {
                //Desactivas Airbot
                mapRef.isActive = false;
                //Activas Camaras
                machineRef.LookAt = boots.transform;
                machineRef.Follow = boots.transform;
                //Activas el Dron
                bootsRef.isActive = true;
                actualCharacter = charSelected;
            }
        }
        else
        {
            if (mapRef.isDeployed)
            {

                if (weinerRef.currentHP > 1)
                {
                    if (actualCharacter == "Weiner")
                    {
                        ///Desactivas Weiner
                        weinerRef.enabled = false;
                        weinerCam.SetActive(false);
                        weinercamControl.enabled = false;
                        //Activas Camaras
                        dronesCam.SetActive(true);
                        cinemachineobj.SetActive(true);
                        machineRef.LookAt = boots.transform;
                        machineRef.Follow = boots.transform;
                        ///Activas el Dron
                        bootsMotor.enabled = true;
                        bootsRef.enabled = true;
                        bootsRef.isActive = true;
                        bootsRef.isDeployed = true;
                        //Le das vida al dron
                        bootsRef.health = 1;
                        weinerRef.currentHP -= 1;
                        // desanclas al dron
                        boots.gameObject.transform.parent = null;
                        boots.transform.position = spawnPosition;
                        actualCharacter = charSelected;
                    }
                    if (actualCharacter == "Airbot")
                    {
                        mapRef.isActive = false;
                        //Activas Camaras
                        //dronesCam.SetActive(true);
                        //cinemachineobj.SetActive(true);
                        machineRef.LookAt = boots.transform;
                        machineRef.Follow = boots.transform;
                        ///Activas el Dron
                        bootsMotor.enabled = true;
                        bootsRef.enabled = true;
                        bootsRef.isActive = true;
                        bootsRef.isDeployed = true;
                        //Le das vida al dron
                        bootsRef.health = 1;
                        weinerRef.currentHP -= 1;
                        // desanclas al dron
                        boots.gameObject.transform.parent = null;
                        boots.transform.position = spawnPosition;
                        actualCharacter = charSelected;
                    }
                   
                }
                
            }
            else
            {
                if (weinerRef.currentHP > 2)
                {
               //     print("setear a boots");

                    weinerRef.enabled = false;
                    weinerCam.SetActive(false);
                    weinercamControl.enabled = false;
                    bootsMotor.enabled = true;
                    bootsRef.enabled = true;

                    dronesCam.SetActive(true);
                    cinemachineobj.SetActive(true);
                    machineRef.LookAt = boots.transform;
                    machineRef.Follow = boots.transform;

                    bootsRef.isDeployed = true;
                    bootsRef.isActive = true;

                    
                    
                    bootsRef.health = 1;
                    mapRef.health = 1;
                    weinerRef.currentHP -= 2;
                    boots.gameObject.transform.parent = null;
                    boots.transform.position = spawnPosition;
                    actualCharacter = charSelected;
                    
                }
            }
        }
    }
    void ActivateAirbot()
    {
        if (mapRef.isDeployed)
        {
            
            if (actualCharacter == "Weiner")
            {
                weinerRef.enabled = false;
                weinerCam.SetActive(false);
                weinercamControl.enabled = false;
                //mapMotor.enabled = true;
                //mapRef.enabled = true;

                dronesCam.SetActive(true);
                cinemachineobj.SetActive(true);
                machineRef.LookAt = map.transform;
                machineRef.Follow = map.transform;

                mapRef.isActive = true;
                actualCharacter = charSelected;
            }
            if (actualCharacter == "Groundbot")
            {
                bootsRef.isActive = false; // DESACTIVAMOS GROUNDBOT

                dronesCam.SetActive(true);
                cinemachineobj.SetActive(true);
                machineRef.LookAt = map.transform;
                machineRef.Follow = map.transform;

                mapRef.isActive = true;
                actualCharacter = charSelected;
            }
        }
        else
        {
            if (bootsRef.isDeployed)
            {
                if (actualCharacter == "Groundbot")
                {
                    
                    Vector3 spawnPosition = new Vector3(boots.transform.position.x, boots.transform.position.y + 3, boots.transform.position.z);

                    bootsRef.isActive = false;

                    //dronesCam.SetActive(true);
                    //cinemachineobj.SetActive(true);
                    machineRef.LookAt = map.transform;
                    machineRef.Follow = map.transform;

                    //Activamos El dron
                    mapMotor.enabled = true;
                    mapRef.enabled = true;
                    mapRef.isDeployed = true;
                    mapRef.isActive = true;
                    //Des anclamos el dron
                    map.gameObject.transform.parent = null;
                    actualCharacter = charSelected;
                    map.transform.position = spawnPosition;
                    isMapwithBoots = false;
                }
                if (actualCharacter == "Weiner")
                {

                    Vector3 spawnPosition = new Vector3(boots.transform.position.x, boots.transform.position.y + 1.5f, boots.transform.position.z);
                    ///ApagasWeiner
                    weinerRef.enabled = false;
                    weinerCam.SetActive(false);
                    weinercamControl.enabled = false;
                    ///Setear encendido de drone aereo
                    mapRef.enabled = true;
                    mapMotor.enabled = true;
                    mapRef.isDeployed = true;
                    mapRef.isActive = true;
                    //Activar camara de Dron
                    dronesCam.SetActive(true);
                    cinemachineobj.SetActive(true);
                    machineRef.LookAt = map.transform;
                    machineRef.Follow = map.transform;
                    //Desanclar Dron
                    map.gameObject.transform.parent = null;
                    actualCharacter = charSelected;
                    map.transform.position = spawnPosition;
                    isMapwithBoots = false;
                }
                
            }
            else
            {
                ////Solo hay una opcion y es que tengas el control de weiner
                Vector3 spawnPosition = new Vector3(weiner.transform.position.x, weiner.transform.position.y+1.5f, weiner.transform.position.z);
                weinerRef.enabled = false;
                weinerCam.SetActive(false);
                weinercamControl.enabled = false;
                mapMotor.enabled = true;
                mapRef.enabled = true;

                dronesCam.SetActive(true);
                cinemachineobj.SetActive(true);
                machineRef.LookAt = map.transform;
                machineRef.Follow = map.transform;

                mapRef.isDeployed = true;
                mapRef.isActive = true;

                
                //print("Setear map");
                ////
                mapRef.health = 1;
                weinerRef.currentHP -= 1;
                map.gameObject.transform.parent = null;
                actualCharacter = charSelected;
                map.transform.position = spawnPosition;
                isMapwithBoots = false;
            }
        }

    }
    void ActivateWeiner()
    {
        if (actualCharacter == "Groundbot")
        {
            weinerRef.enabled = true;
            weinerCam.SetActive(true);
            weinercamControl.enabled = true;
            //mapMotor.enabled = false;
            //mapRef.enabled = false;
            dronesCam.SetActive(false);
            cinemachineobj.SetActive(false);
            bootsRef.isActive = false; ///variable a Observar
            actualCharacter = charSelected;

        }
        if (actualCharacter == "Airbot")
        {
            weinerRef.enabled = true;
            weinerCam.SetActive(true);
            weinercamControl.enabled = true;
            //mapMotor.enabled = false;
            //mapRef.enabled = false;
            dronesCam.SetActive(false);
            cinemachineobj.SetActive(false);
            mapRef.isActive = false; ///variable a Observar
            actualCharacter = charSelected;
        }
    }

    // Replegar Drones:
    public void ReplegarBoots()
    {
        if (mapRef.isDeployed)
        {
            boots.transform.parent = weiner.transform;
            boots.transform.localPosition = bootsSavePos;
            boots.transform.localEulerAngles = new Vector3(0, 0, 0);
            bootsRef.health -= 1;
            weinerRef.currentHP += 1;
            bootsRef.isDeployed = false;
        }
        if (!mapRef.isDeployed)
        {
            if (isMapwithBoots)
            {
                boots.transform.parent = weiner.transform;
                boots.transform.localPosition = bootsSavePos;
                boots.transform.localEulerAngles = new Vector3(0, 0, 0);
                bootsRef.health -= 1;
                mapRef.health -= 1;
                weinerRef.currentHP += 2;
                bootsRef.isDeployed = false;
            }
            if (!isMapwithBoots)
            {
                boots.transform.parent = weiner.transform;
                boots.transform.localPosition = bootsSavePos;
                boots.transform.localEulerAngles = new Vector3(0, 0, 0);
                map.transform.parent = boots.transform;
                map.transform.localPosition = mapSavePos;
                map.transform.localEulerAngles = new Vector3(0, 0, 0);
                bootsRef.health -= 1;
                weinerRef.currentHP += 1;
                bootsRef.isDeployed = false;

            }
            
        }

    }

    public void ReplegarMap()
    {
        if (bootsRef.isDeployed)
        {
            map.transform.parent = weiner.transform;
            map.transform.localPosition = new Vector3 (0,1,-0.25f);
            map.transform.localEulerAngles = new Vector3(0, 0, 0);
            mapRef.health -= 1;
            weinerRef.currentHP += 1;
            mapRef.isDeployed = false;
        }
        if (!bootsRef.isDeployed)
        {
            map.transform.parent = boots.transform;
            map.transform.localPosition = mapSavePos;
            map.transform.localEulerAngles = new Vector3(0, 0, 0);
            mapRef.health -= 1;
            weinerRef.currentHP += 1;
            mapRef.isDeployed = false;

        }
    }
}
