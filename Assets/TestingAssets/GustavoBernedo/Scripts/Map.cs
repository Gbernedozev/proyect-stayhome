﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : Drones, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }


    public GameObject interacter;




    public void TriggerInteraction()
    {
        if (Interactable && !IsActive)
        {
            Debug.Log("PICK UP MAPS!!");
            HudController.hud.HideAdvert();
            //    LevelManager.levelManager.checkPointIndex = idCheckpoint;
            //    GameStatusChanger.gameStatus.SetNewSpawnPoint(idCheckpoint);
            //    WeinerController.player.currentHP = WeinerController.player.hp;
            //    elevatorAnnexed.IsActive = true;
        }
        else
        {
            Debug.Log("ERROR. NO POWER");
        }

    }


    float z;
    float x;
    float y;
    float turnSmoothTime = 0.1f;
    float turnSmoothVelocity = 1f;

    Vector3 velDirection = new Vector3();

    CharacterController characterController;

    float downVelocity = 5f;
    float upVelocity=5f;
    float navigationVelocity = 8f;
    //Inputs:
    bool inputUP;
    bool inputDown;
    bool inputInteracting;

    //Estados del personaje
    bool isInteracting;

    //Raycast
    float castrange = 1;
    public LayerMask interactionMask;


    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        //   Interactable = !IsActive;
        if (isDeployed && !isActive)
        {
            interacter.SetActive(true);
        }

        if (isDeployed && isActive)
        {
            interacter.SetActive(false);
        }

        MomentUpDown();
        MovementNavigation(navigationVelocity);
        AirDronInteraction();
    }

    void MovementNavigation(float vel)
    {
        z = Input.GetAxisRaw("Vertical");
        x = Input.GetAxisRaw("Horizontal");

        velDirection = new Vector3(x, 0f, z).normalized;

        if (velDirection.magnitude >= 0.1f && isActive)
        {
            float targetAngle = Mathf.Atan2(velDirection.x, velDirection.z) * Mathf.Rad2Deg + cam.eulerAngles.y; // Sacas el Angulo y conviertes de radianes a grados
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime); //generas una transicion al girar

            transform.rotation = Quaternion.Euler(0f, angle, 0f); // Rotacion del personaje

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            characterController.Move(moveDirection.normalized * vel * Time.deltaTime);


        }
    }

    void MomentUpDown()
    {
        inputUP = Input.GetButton("Jump");
        inputDown = Input.GetKey(KeyCode.LeftShift);
        Vector3 inputVector = new Vector3(0,y,0).normalized;
        
        if (inputUP)
        {
            if (isActive)
            {
                y = Input.GetAxisRaw("Jump");
                characterController.Move(inputVector * upVelocity * Time.deltaTime);
            }
            
        }
        if(inputDown)
        {
            if (isActive)
            {
                y = Input.GetAxisRaw("Jump");
                characterController.Move(inputVector * -downVelocity * Time.deltaTime);

            }
        }
        
    }

    void AirDronInteraction()
    {
        inputInteracting = Input.GetKeyDown(KeyCode.F);

        if (inputInteracting)
        {
            RaycastHit hit;

            if (!isInteracting)
            {
                isInteracting = true;
            }

            if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, castrange, interactionMask))
            {
                Debug.Log(hit.collider.gameObject);
                int layer = hit.collider.gameObject.layer;
                IObjectInteractable objectInteractable = hit.collider.gameObject.GetComponent<IObjectInteractable>();

                if (objectInteractable.Interactable)
                {
                    switch (layer)
                    {
                        case 8:
                            print("Regresa a player");
                            break;
                        case 16:
                            print("Interactuando con panel terrestre"); // OK
                            break;

                    }
                    objectInteractable.TriggerInteraction();
                }


            }
        }
        else
        {
            if (isInteracting)
            {
                isInteracting = false;
            }
        }

    }


}