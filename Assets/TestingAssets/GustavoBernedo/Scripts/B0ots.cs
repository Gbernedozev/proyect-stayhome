﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B0ots : Drones, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }


    public GameObject interacter;

    public void TriggerInteraction()
    {
        if (Interactable && !IsActive)
        {
            Debug.Log("PICK BOOTS!!");
            HudController.hud.HideAdvert();
            //    LevelManager.levelManager.checkPointIndex = idCheckpoint;
            //    GameStatusChanger.gameStatus.SetNewSpawnPoint(idCheckpoint);
            //    WeinerController.player.currentHP = WeinerController.player.hp;
            //    elevatorAnnexed.IsActive = true;
        }
        else
        {
            Debug.Log("ERROR. NO POWER");
        }

    }

   
    float z;
    float x;
    float turnSmoothTime = 0.1f;
    float turnSmoothVelocity = 1f;

    Vector3 velDirection = new Vector3();

    //Deteccion de 
    Vector3 velocity; // Velocidad de caida

    public Transform groundCheck;
    public float groundDistance = 0.2f;
    public LayerMask groundMask;
    bool isGrounded;
    float gravity = -9.81f;

    //
    CharacterController characterController;

    //Estados
    bool isInteracting;
    //Inputs:
    bool inputInteracting;
    //Raycast
    float castrange = 1;
    public LayerMask interactionMask;

    // Start is called before the first frame update
    void Start()
    {
        
        health = 0;
        Cursor.lockState = CursorLockMode.Locked;
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
    //    Interactable = !IsActive;

        if (isDeployed && isActive)
        {
            Movement(speed);
            CheckSphere();
            GroundDronInteraction();
            interacter.SetActive(false);
        }

        if(isDeployed && !isActive)
        {
            interacter.SetActive(true);
        }
           
    }
    void Movement(float vel)
    {
        z = Input.GetAxisRaw("Vertical");
        x = Input.GetAxisRaw("Horizontal");

        velDirection = new Vector3(x, 0f, z).normalized;

        if (velDirection.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(velDirection.x, velDirection.z) * Mathf.Rad2Deg + cam.eulerAngles.y; // Sacas el Angulo y conviertes de radianes a grados
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime); //generas una transicion al girar

            transform.rotation = Quaternion.Euler(0f, angle, 0f); // Rotacion del personaje

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            characterController.Move(moveDirection.normalized * vel * Time.deltaTime);

            velocity.y += gravity * Time.deltaTime;

            characterController.Move(velocity * Time.deltaTime);
        }


    }
    public void CheckSphere()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f; 
        }
    }

    void GroundDronInteraction()
    {
        inputInteracting = Input.GetKeyDown(KeyCode.F);

        if (inputInteracting)
        {
            RaycastHit hit;

            if (!isInteracting)
            {
                isInteracting = true;
            }

            if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, castrange, interactionMask))
            {
                Debug.Log(hit.collider.gameObject);
                int layer = hit.collider.gameObject.layer;
                IObjectInteractable objectInteractable = hit.collider.gameObject.GetComponent<IObjectInteractable>();

                if (objectInteractable.Interactable)
                {
                    switch (layer)
                    {
                        case 8:
                            print("Regresa a player");
                            break;
                        case 17:
                            print("Interactuando con panel terrestre"); // OK
                            break;
                            
                       
                    }
                    objectInteractable.TriggerInteraction();
                }


            }
        }
        else
        {
            if (isInteracting)
            {
                isInteracting = false;
            }
        }

    }

}
