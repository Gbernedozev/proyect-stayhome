﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapHud : MonoBehaviour
{
    WeinerController player;
    CharManager manager;

    [Header("Elementos del hud")]
    public Image[] icons;

    //Var for updating character selected:
    string lastCharacter;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<WeinerController>();
        manager = GameObject.FindGameObjectWithTag("CharControl").GetComponent<CharManager>();

        lastCharacter = manager.charSelected;

    }

    // Update is called once per frame
    void Update()
    {
        if (manager.actualCharacter == "Airbot")
        {
            //Updating Dron selector

            if (lastCharacter != manager.charSelected)
            {

                CharSelectorUpdater();
                lastCharacter = manager.charSelected;
            }

        }
    }

    public void CharSelectorUpdater()
    {
        if (manager != null)
        {
            if (manager.charSelected == "Weiner")
            {
                Image imagen = icons[0].GetComponent<Image>();
                imagen.CrossFadeColor(Color.yellow, 0.4f, true, true);

                Image imagen2 = icons[1].GetComponent<Image>();
                imagen2.CrossFadeColor(Color.white, 0.4f, true, true);

            }


            if (manager.charSelected == "Groundbot")
            {
                Image imagen = icons[0].GetComponent<Image>();
                imagen.CrossFadeColor(Color.white, 0.4f, true, true);

                Image imagen2 = icons[1].GetComponent<Image>();
                imagen2.CrossFadeColor(Color.yellow, 0.4f, true, true);
            }

        }
        else
        {
            Image imagen = icons[0].GetComponent<Image>();
            imagen.CrossFadeAlpha(50, 0.1f, false);

            Image imagen2 = icons[1].GetComponent<Image>();
            imagen2.CrossFadeAlpha(50, 0.1f, false);

        }

    }
}
