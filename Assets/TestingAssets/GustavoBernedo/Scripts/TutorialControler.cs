﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControler : MonoBehaviour
{

    public GameObject[] tutoriales;
    public GameObject Panel;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    

    public void ActivateTutorial(int indice)
    {
        Panel.SetActive(true);
        for (int i = 0; i < tutoriales.Length; i++)
        {
            if (i != indice)
                tutoriales[i].SetActive(false);
            
            
            if (i == indice)
            {
                print("se activa");
                tutoriales[i].SetActive(true);
            }
            
        }
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }

    public void DeactivateTutorial()
    {
        for (int i = 0; i < tutoriales.Length; i++)
        {
            tutoriales[i].SetActive(false);
        }
        Panel.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
    }


   
}