﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterView : Character
{
    public static CharacterView humanPlayer;

    private void Awake()
    {
        isActive = true;
        humanPlayer = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //isActive = true;
        currentHealth = maxhealth;
        speed = 8f;
    }

    // Update is called once per frame
    void Update()
    {
        CheckSphere();
        RegularMovement();
    }


}
