﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugNew : MonoBehaviour
{
    bool show;
    GameObject player;
    public DoorManager doorManager;
    public TerminalManager terminalManager;
    public CheckPoints checkPoints;

    public GameObject enemy1;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if(GameStatusChanger.gameStatus.gameState == GameStatusChanger.GameState.Debug)
            {
                GameStatusChanger.gameStatus.ResumeGame();
            }
            else
            {
                GameStatusChanger.gameStatus.ShowDebugWindow();
            }
            */
        /*    if (ui_status_changer.GetComponent<GameStatusChanger>().global_ui_status == "debug")
            {
                ui_status_changer.GetComponent<GameStatusChanger>().UnpauseAndChangeToPlayingUI();
            }
            else
            {
                ui_status_changer.GetComponent<GameStatusChanger>().PauseAndDebug();
            }*/
       
    }


    public void TeleportToTutorial()
    {
        SceneManager.LoadScene("Tutorial");
       // ui_status_changer.GetComponent<GameStatusChanger>().UnpauseAndChangeToPlayingUI();
    }

    public void TeleportToLevel1()
    {
        SceneManager.LoadScene("Lv. 1 (Mitad Beta)");
        //ui_status_changer.GetComponent<GameStatusChanger>().UnpauseAndChangeToPlayingUI();

    }

    public void TeleportToLevel2()
    {
        SceneManager.LoadScene("Lv.1 (Mitad WB)");
        //ui_status_changer.GetComponent<GameStatusChanger>().UnpauseAndChangeToPlayingUI();

    }

    public void ReduceHealth()
    {
        WeinerController.player.currentHP--;
    //    player.GetComponent<CharacterView>().ReduceHealth(1);
    }

    public void RestoreMaxHealth()
    {
        WeinerController.player.currentHP = WeinerController.player.hp;
   //     player.GetComponent<CharacterView>().RestoreFullHealth();
    }

  /*  public void RespawnAtStart()
    {
        ui_status_changer.GetComponent<GameStatusChanger>().UnpauseAndChangeToPlayingUI();
        checkPoints.Respawn(0);
    }*/

    public void EnableDoors()
    {
        doorManager.EnableAllDoors();
    }

    public void ActivateAllTerminals()
    {
        terminalManager.EnableAllTerminals();
    }

    public void DisableAllTerminals()
    {
        terminalManager.DisableAllTerminals();
    }

    public void ShowEnemy1()
    {
        Vector3 playerPos = player.transform.position + new Vector3(0f, 2f, 0f);

        Vector3 spawnPos = playerPos + player.transform.forward * 5f;
        Instantiate(enemy1, spawnPos, player.transform.rotation);
    }

    public void IncreaseNodes()
    {
        if (WeinerController.player.currentNodos <5)
        {
            WeinerController.player.currentNodos++;
        }
    }

    public void DeceaseNodes()
    {
        if(WeinerController.player.currentNodos > 0)
        {
            WeinerController.player.currentNodos--;
        }
    }
}
