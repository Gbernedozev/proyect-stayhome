﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebuggerOptions : MonoBehaviour
{
    public GameObject playerControlPanel;
    public GameObject groundControlPanel;
    public GameObject airControlPanel;
    // Start is called before the first frame update
    void Start()
    {
         
    }

    // Update is called once per frame
    void Update()
    {
        ShowControls();
    }

    void ShowControls()
    {
        switch (PlayerManager.playerManager.activeCharacter)
        {
            case PlayerManager.ControlCharacter.Human:
                playerControlPanel.SetActive(true);
                groundControlPanel.SetActive(false);
                airControlPanel.SetActive(false);
                break;
            case PlayerManager.ControlCharacter.EarthBot:
                playerControlPanel.SetActive(false);
                groundControlPanel.SetActive(true);
                airControlPanel.SetActive(false);
                break;
            case PlayerManager.ControlCharacter.AirBot:
                playerControlPanel.SetActive(false);
                groundControlPanel.SetActive(false);
                airControlPanel.SetActive(true);
                break;
        }

   /*     if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myPlayer.model.HP--;
            Debug.Log(myPlayer.model.HP);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            myPlayer.model.HP++;
            Debug.Log(myPlayer.model.HP);
        }*/
    }
}
