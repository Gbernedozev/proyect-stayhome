﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController myCamera;

    Transform target;
    public Vector3 offsetPos;
    public float moveSpeed;
    public float turnSpeed;
    public float smoothSpeed;

    Quaternion targetRot;
    Vector3 targetPos;
    bool smoothRotation;

    public bool lookAtTarget;
    public bool rotateAroundPlayer;

    private void Awake()
    {
        myCamera = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        FindCameraTarget();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rotateAroundPlayer)
        {
            Quaternion camTurnAngle = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up);
            offsetPos = camTurnAngle * offsetPos;
        }

        Vector3 newPos = target.position + offsetPos;
        transform.position = Vector3.Slerp(transform.position, newPos, smoothSpeed);

        if (lookAtTarget)
        {
            transform.LookAt(target);
        }

        //    MoveWithTarget();
        //    LookAtTarget();
    }

    void MoveWithTarget()
    {
        targetPos = target.position + offsetPos;
        transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);
    }

    void LookAtTarget()
    {
        targetRot = Quaternion.LookRotation(target.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, turnSpeed * Time.deltaTime);
    }

    private void LateUpdate()
    {


    }

    public void FindCameraTarget()
    {
        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            target = PlayerManager.playerManager.human.transform.Find("Pivot").transform;
        }

        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.EarthBot)
        {
            target = PlayerManager.playerManager.groundBot.transform.Find("Pivot").transform;

        }

        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.AirBot)
        {
            target = PlayerManager.playerManager.airBot.transform.Find("Pivot").transform;
        }
    }
}