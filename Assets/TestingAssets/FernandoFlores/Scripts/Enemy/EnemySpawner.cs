﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
  
    public bool onlyOnce;
    public bool hasSpawned;
    public enum EnemyTypes
    {
        Type1,
        Type2,
        Type3
    };

    public EnemyTypes enemyTypes;
    public string lastSpawnedEnemy;
    // Use this for initialization

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
   
    }
    

    public void CreateEnemy()
    {
        if (enemyTypes == EnemyTypes.Type1)
        {
            CreateType1();
        }
        
        if (enemyTypes == EnemyTypes.Type2)
        {
            CreateType2();
        }
        if (enemyTypes == EnemyTypes.Type3)
        {
            CreateType3();
        }
    }


    void CreateType1()
    {
        // CREATE KAPPA OBJECT
        GameObject enemy = Resources.Load("Enemigos/Type1/Type1") as GameObject;
        GameObject createdEnemy = Instantiate(enemy,transform.position,transform.rotation);
        createdEnemy.name = "Type1";
        lastSpawnedEnemy = enemyTypes.ToString();
        
    }

    void CreateType2()
    {
        // CREATE KAPPA OBJECT
        GameObject enemy = Resources.Load("Enemigos/Type2/Type2") as GameObject;
        GameObject createdEnemy = Instantiate(enemy, transform.position, transform.rotation);
        createdEnemy.name = "Type2";
        lastSpawnedEnemy = enemyTypes.ToString();
    }
    void CreateType3()
    {
        // CREATE KAPPA OBJECT
        GameObject enemy = Resources.Load("Enemigos/Type3/Type3") as GameObject;
        GameObject createdEnemy = Instantiate(enemy, transform.position, transform.rotation);
        createdEnemy.name = "Type3";
        lastSpawnedEnemy = enemyTypes.ToString();
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (onlyOnce)
            {
                if (!hasSpawned)
                {
                    CreateEnemy();
                    hasSpawned = true;
                }
            }
            else
            {
                CreateEnemy();
            }
        }
    }
}