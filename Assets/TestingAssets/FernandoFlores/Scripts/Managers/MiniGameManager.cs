﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MiniGameManager : MonoBehaviour
{
    public static MiniGameManager instance;


    private void Awake()
    {
        instance = this;
    }


    // CONTROL DEL HUD VIDA HUMANO

    // CONTROL DE HUD BOT GROUND

    // CONTROL DE HUD BOT AIRE

    // CONTROL DE MINIGAMES
    public RectTransform closePosition;
    public RectTransform openPosition;

    // CONTROL HACK SIMON
    public SimonManager simonController;
 //   public GameObject simonMinigamePanel;


    // BEHAVIOR SIMON
    public void ShowSimonMinigame()
    {
        simonController.gameObject.transform.DOLocalMove(openPosition.localPosition, 10 * Time.unscaledDeltaTime).SetEase(Ease.InOutElastic).SetUpdate(UpdateType.Normal,true);
    //    simonController.gameObject.GetComponent<CanvasGroup>().DOFade(1, 0.5f * Time.unscaledDeltaTime);

        GameStatusChanger.gameStatus.ShowMinigameWindow();

    //    GameStatusChanger.gameStatus.EnableMiniGame();//just testing
    }

    public void HideSimonMinigame()
    {
        simonController.gameObject.transform.DOLocalMove(closePosition.localPosition, 10 * Time.unscaledDeltaTime).SetEase(Ease.InOutElastic).SetUpdate(UpdateType.Normal, true);
    //    simonController.gameObject.GetComponent<CanvasGroup>().DOFade(0, 0.5f * Time.unscaledDeltaTime);
        //    GameStatusChanger.gameStatus.UnpauseAndChangeToPlayingUI();

        //GameStatusChanger.gameStatus.ResumeGame();
        GameStatusChanger.gameStatus.ResumeNormalPlay();
    }

   
}
