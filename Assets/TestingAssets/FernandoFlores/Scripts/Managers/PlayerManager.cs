﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager playerManager;
    public PlayerController human;
    public EarthBotController groundBot;
    public AirBotController airBot;
    public CameraController controlCamera;

    public ControlCharacter activeCharacter = ControlCharacter.Human;

    public enum ControlCharacter
    {
        Human,
        EarthBot,
        AirBot
    }

    private void Awake()
    {
        playerManager = this;
    /*    if (playerManager == null)
        {
            DontDestroyOnLoad(gameObject);
            playerManager = this;
        }
        else if (playerManager != this)
        {
            Destroy(gameObject);
        }*/
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SwitchToGround()
    {
        activeCharacter = ControlCharacter.EarthBot;
        controlCamera.FindCameraTarget();
        groundBot.TakeControl();
        
    }

    public void SwitchToAir()
    {
        activeCharacter = ControlCharacter.AirBot;
        controlCamera.FindCameraTarget();
        airBot.TakeControl();
    }

    public void SwitchToHuman()
    {
        activeCharacter = ControlCharacter.Human;
        controlCamera.FindCameraTarget();
    }

    public bool IsGrounded(Collider objectCollider)
    {
        float extraH = 0.1f;
        bool raycastHit = Physics.BoxCast(objectCollider.bounds.center + new Vector3(0, 0.1f, 0), objectCollider.bounds.extents, Vector3.down, objectCollider.transform.rotation, extraH);
        return raycastHit;
    }
}
