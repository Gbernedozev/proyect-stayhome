﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTrigger : MonoBehaviour
{
 //   public int endLevelID;
    public bool canTriggerExit;
    public LevelData level;
 //   public string nextLevel;

    public TypeTransition typeTransition;
    public enum TypeTransition
    {
        ToNextLevel,
        ToPrevLevel
    }
    
    // Update is called once per frame
    void Update()
    {
        

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            DataKeeper.healKeerper = WeinerController.player.currentHP;
            DataKeeper.nodesKeeper = WeinerController.player.currentNodos;
            DoTransition();
            LevelManager.levelManager.LoadSpecificLevel(level.levelName);
        }

    }

    void DoTransition()
    {
        switch (typeTransition)
        {
            case TypeTransition.ToNextLevel:
                LevelManager.levelManager.checkPointIndex = 0;
                break;
            case TypeTransition.ToPrevLevel:
                LevelManager.levelManager.checkPointIndex = level.lastSpawnIndex;
                break;
            default:
                break;
        }
        

    }
}
