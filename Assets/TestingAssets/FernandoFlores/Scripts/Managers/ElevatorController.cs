﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }



    public Animator anim;


    public bool doorOpen;

    public Renderer lightDoor;
    public Material lightOn;
    public Material lightOff;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            lightDoor.material = lightOn;
        }
        else
        {
            lightDoor.material = lightOff;
        }
    }

    public void TriggerInteraction()
    {
        if (isActive)
        {
            doorOpen = !doorOpen;
            anim.SetBool("isSomeone", doorOpen);
        }
        else
        {
            HudController.hud.ShowWarning("OFFLINE. PLEASE TURN ON POWER");
          
        }
    }
}
