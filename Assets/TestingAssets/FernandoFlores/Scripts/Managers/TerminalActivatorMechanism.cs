﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalActivatorMechanism : MonoBehaviour
{
    public GameObject objectToActivate;

    public PanelController[] panels;

    public bool activated;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!activated && TerminalState())
        {
            CheckObjectActivation();
        }
    }

    bool TerminalState()
    {
        for (int i = 0; i < panels.Length; i++)
        {
            if(panels[i].isActive == false)
            {
                return false;
            }
        }

        return true;
    }


    public void CheckObjectActivation()
    {
        switch (objectToActivate.tag)
        {
            case "Door":
                objectToActivate.transform.Find("Activator").GetComponent<Sensor>().isActive = true;
                break;
            case "Terminal":
                objectToActivate.GetComponent<IObjectInteractable>().IsActive = true;
                break;
        }
        activated = true;

    //    terminal.isActive = TerminalState();
    }
}
