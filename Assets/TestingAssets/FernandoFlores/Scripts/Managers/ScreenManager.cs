﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour
{
    public void LoadLevelTutorial()
    {
        SceneManager.LoadSceneAsync("Tutorial", LoadSceneMode.Single);
    }
    public void LoadLevelMainMenu()
    {
        SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
    }

    public void LoadOptions()
    {
        Debug.Log("OPTIONS,BRUH");
    //    SceneManager.LoadSceneAsync("Options", LoadSceneMode.Single);
    }

    public void ReloadScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }


    public void ExitGame()
    {
        Application.Quit();
    }
}
