﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFader : MonoBehaviour
{
    public Animator endLevelTransition;
    public Animator redTransition;

    public static ScreenFader screenFader;

    private void Awake()
    {
        screenFader = this;
    }

    public void ActivateTransition()
    {
        Debug.Log("FadeOut");
        endLevelTransition.SetBool("Fade", true);
    }

    public void DesactivateTransition()
    {
        endLevelTransition.SetBool("Fade", false);
    }

    public void ActRedTransition()
    {
        redTransition.SetBool("Fading", true);
    }

    public void DesactRedTransition()
    {
        redTransition.SetBool("Fading", false);
    }
}
