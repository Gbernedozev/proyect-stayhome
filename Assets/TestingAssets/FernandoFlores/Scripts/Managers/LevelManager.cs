﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour
{
    public static LevelManager levelManager;
    // public ScreenFader screenFader;
    [Header("Todos los niveles")]
    public List<string> levels;
    public int indexLevel = 0;

    [Header("Counter de CheckPoints")]
    public int checkPointIndex = 0;
    [Header("Time to Fade Out")]
    public float timeToFadeOut;
    // Start is called before the first frame update

    private void Awake()
    {
        if (levelManager == null)
        {
            DontDestroyOnLoad(gameObject);
            levelManager = this;
        }
        else if (levelManager != this)
        {
            Destroy(gameObject);
        }
    }


    public void LoadSpecificLevel(string level)
    {
        indexLevel = levels.IndexOf(level);
        ScreenFader.screenFader.ActivateTransition();
        StartCoroutine(LevelLoader(level));
    }

    IEnumerator LevelLoader(string scene)
    {
        yield return new WaitForSeconds(timeToFadeOut);
        SceneManager.LoadScene(scene);
    }
    
    public void LoadDebugLevel(string level)
    {
        ScreenFader.screenFader.ActivateTransition();
        StartCoroutine(LevelLoader(level));
    }
   
}
