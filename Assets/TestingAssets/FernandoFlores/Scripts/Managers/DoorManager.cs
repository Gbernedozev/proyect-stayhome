﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    public GameObject[] doorList;

    // Start is called before the first frame update
    void Start()
    {
        FindAllDoorsInScene();
    }

    void FindAllDoorsInScene()
    {
        doorList = GameObject.FindGameObjectsWithTag("Door");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public void DisableAllDoors()
    {
        for (int i = 0; i < doorList.Length; i++)
        {
            doorList[i].transform.Find("Activator").GetComponent<Sensor>().isActive = false;
           
        }
    }

    public void EnableAllDoors()
    {
        for (int i = 0; i < doorList.Length; i++)
        {
            doorList[i].transform.Find("Activator").GetComponent<Sensor>().isActive = true;
        }
    }

}
