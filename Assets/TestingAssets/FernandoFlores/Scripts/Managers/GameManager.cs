﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    // PARA SPAWN
    public int indexCheckPoint = 0;
    // GAME MANAGER DEBE ENCARGARSE DEL RESPAWN

    // QUE NECESITO PARA RESPAWN

    // CHECKPOINTS
    // ID DE CHECKPOINT

    private void Awake()
    {
        if (gameManager == null)
        {
            DontDestroyOnLoad(gameObject);
            gameManager = this;
        }
        else if (gameManager != this)
        {
            Destroy(gameObject);
        }
        

    }
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            ForceRespawn();
        }
    }

    public void SpawnPlayer()
    {
        // TRANSFORM
    }

    public void ForceRespawn()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
