﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodePanel : MonoBehaviour, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }

    public Sensor elevatorAnnexed;

    public Renderer panelColor;

    public Material interactedColor;

    public bool hasAwarded;
    public int amountNodesAward;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TriggerInteraction()
    {
        if (isActive)
        {
            if (!hasAwarded)
            {
                WeinerController.player.currentNodos += amountNodesAward;
                panelColor.material = interactedColor;
                elevatorAnnexed.OpenDoor();
            }
        }
        else
        {
            HudController.hud.ShowWarning("ERROR. NO POWER");
        }

    }

}
