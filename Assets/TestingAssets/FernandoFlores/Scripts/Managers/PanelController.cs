﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public enum TypeBot
    {
        GroundBot,
        AirBot
    }

    public bool isActive;

   // public TerminalActivatorMechanism baseTerminal;


    public TypeBot typeBot;

    public Renderer panel;
    public Material activated;

    private void OnTriggerEnter(Collider other)
    {
        CheckCollision(other.gameObject);
    }


    void CheckCollision(GameObject hit)
    {
        switch (typeBot)
        {
            case TypeBot.GroundBot:
                if(hit.tag == "GroundBot")
                {
                    ActivatePanel();
                    GetComponentInParent<AudioClipPlayer>().PlayAudio(0, 1.1f);
                }
                break;
            case TypeBot.AirBot:
                if(hit.tag == "Airbot")
                {
                    ActivatePanel();
                    GetComponentInParent<AudioClipPlayer>().PlayAudio(0, 1.1f);
                }
                break;
        }
    }


    void ActivatePanel()
    {
        if (!isActive)
        {
            isActive = true;
            panel.material = activated;
        //    baseTerminal.CheckObjectActivation();
        }
    }
    
}
