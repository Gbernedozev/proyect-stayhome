﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractChecker : MonoBehaviour
{
    public GameObject objectInteractable;
    IObjectInteractable target;
    // Start is called before the first frame update
    void Start()
    {
        target = objectInteractable.GetComponent<IObjectInteractable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            string layerName = LayerMask.LayerToName(objectInteractable.layer);
            other.GetComponent<WeinerController>().mask = LayerMask.GetMask(layerName);
            target.Interactable = true;
            HudController.hud.RevealAdvert();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<WeinerController>().mask = LayerMask.GetMask("Default");
            target.Interactable = false;
            HudController.hud.HideAdvert();
        }

    }

}
