﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalManager : MonoBehaviour
{
    public GameObject[] terminalList;


    // Start is called before the first frame update
    void Start()
    {
        AllTerminals();
    }

    void AllTerminals()
    {
        terminalList = GameObject.FindGameObjectsWithTag("Terminal");
    }

    public void DisableAllTerminals()
    {
        for (int i = 0; i < terminalList.Length; i++)
        {
            terminalList[i].GetComponent<IObjectInteractable>().IsActive = false;
        }
    }

    public void EnableAllTerminals()
    {
        for (int i = 0; i < terminalList.Length; i++)
        {
            terminalList[i].GetComponent<IObjectInteractable>().IsActive = true;
        }
    }


}
