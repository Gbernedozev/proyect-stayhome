﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoints : MonoBehaviour
{
    public List<Transform> checkPointPosition;
    public GameObject player;


    private void Start()
    {
    //    player = GameObject.FindGameObjectWithTag("Player");
        player.transform.position = checkPointPosition[LevelManager.levelManager.checkPointIndex].position;
    }

    public void Respawn(int index)
    {
        player.transform.position = checkPointPosition[index].position;
    }

}
