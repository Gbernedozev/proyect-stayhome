﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitFinal : MonoBehaviour, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }
    
    public Renderer[] circuitObjects;
        
    public Material on;
    public Material off;
    public Material activated;

    public enum CircuitActivate
    {
        Elevator,
        Interactable
    }

    public CircuitActivate circuit;

    public GameObject objectToActivate;

    public bool hasBeenTurnedOn;


    public void TriggerInteraction()
    {
        if (IsActive)
        {
            if (!hasBeenTurnedOn && WeinerController.player.currentNodos>0)
            {
                Debug.Log("Activate");
                WeinerController.player.currentNodos--;
                ActivateCircuit();
                ColorRenderers(activated);
                hasBeenTurnedOn = true;
            }
        }
        else
        {
            Debug.Log("OFFLINE. PLEASE TURN ON POWER");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasBeenTurnedOn)
        {
            if (isActive)
            {
                ColorRenderers(on);
            //    circuitObject.material = on;
                //    ActivateCircuit();
                //   objectToActivate.GetComponent<IObjectInteractable>().IsActive = true;
            }
            else
            {
                ColorRenderers(off);
                //    circuitObject.material = off;
                //    CloseCircuit();
                //    objectToActivate.GetComponent<IObjectInteractable>().IsActive = false;
            }
        }
        
    }

    void ColorRenderers(Material mat)
    {
        for (int i = 0; i < circuitObjects.Length; i++)
        {
            circuitObjects[i].material = mat;
        }
    }

    void ActivateCircuit()
    {
        switch (circuit)
        {
            case CircuitActivate.Elevator:
                objectToActivate.GetComponent<Sensor>().isActive = true;
                break;

            case CircuitActivate.Interactable:
                objectToActivate.GetComponent<IObjectInteractable>().IsActive = true;
                break;
            default:
                break;
        }
    }


    void CloseCircuit()
    {
        switch (circuit)
        {
            case CircuitActivate.Elevator:
                objectToActivate.GetComponent<Sensor>().isActive = false;
                break;

            case CircuitActivate.Interactable:
                objectToActivate.GetComponent<IObjectInteractable>().IsActive = false;
                break;
            default:
                break;
        }
    }
}
