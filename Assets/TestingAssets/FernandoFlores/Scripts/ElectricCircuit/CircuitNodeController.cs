﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitNodeController : MonoBehaviour,IObjectInteractable
{

    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    public enum PossibleOptions
    {
        Up,
        Right,
        Down,
        Left
    }

    public int optionIndex;

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }

    public PossibleOptions options;

    public PossibleOptions rightAnswer;

    public GameObject node;
    public Renderer circuit;
    public Material lightOn;
    public Material lightOff;

    public GameObject nextCircuit; // PUEDEN SER VARIOS
    public enum NextCircuit
    {
        Node,
        Last
    }

    public NextCircuit next;

    // Start is called before the first frame update
    void Start()
    {
        optionIndex = (int)options;
    }

    // Update is called once per frame
    void Update()
    {
        if(options == rightAnswer)
        {
            circuit.material = lightOn;
            CheckNextActivation(true);
            // ENABLE NEXT
        }
        else
        {
            circuit.material = lightOff;
            CheckNextActivation(false);
        }
    }

    public void TriggerInteraction()
    {
        if (IsActive)
        {
            optionIndex++;
            RotateNode();
            if (optionIndex > System.Enum.GetValues(typeof(PossibleOptions)).Length - 1)
            {
                optionIndex = 0;
            }

            options = (PossibleOptions)optionIndex; // ???
        }
        
    }

    void RotateNode()
    {
        node.transform.Rotate(0, 0, -90f);
    }

    void CheckNextActivation(bool output)
    {
        switch (next)
        {
            case NextCircuit.Node:
                nextCircuit.GetComponent<IObjectInteractable>().IsActive = output;
                break;
            case NextCircuit.Last:
                nextCircuit.GetComponent<IObjectInteractable>().IsActive = output;
                break;
            default:
                break;
        }
    }
}
