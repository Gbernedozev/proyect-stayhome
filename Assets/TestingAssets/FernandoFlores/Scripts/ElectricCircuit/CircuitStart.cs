﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitStart : MonoBehaviour, IObjectInteractable
{
    private bool _interactable = true;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }

    public bool hasBeenTurnedOn;

    public Renderer[] circuitBody;
    public Material enabledTerminal;

    // CIRCUIT STARTER
    // IF ACTIVE
    // SET KNOB TO ACTIVE

    // circuit knob
    // can rotate
    // 4 DIR
    // IF MATCH, TURN ACTIVE AND ACTIVATE NEXT NODE


    // circuit end
    // if active
    // object turns active
    
    public GameObject nextCircuit;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // HMMMMM
    }
    

    public void ActivateCircuitNetwork()
    {
        isActive = true;
    }
    

    public void TriggerInteraction()
    {
        if (Interactable)
        {
            if (!hasBeenTurnedOn)
            {
                if (WeinerController.player.currentNodos > 0)
                {
                    WeinerController.player.currentNodos--;
                    Debug.Log("Activate Circuit");
                    ColorTerminal();
                    nextCircuit.GetComponent<IObjectInteractable>().IsActive = true;
                    hasBeenTurnedOn = true;
                }
                else
                {
                    HudController.hud.ShowWarning("Not enough nodes");
                //    Debug.Log("Not enough Ener- I mean, nodes...");
                }

            }
        }
        
        
    }

    void ColorTerminal()
    {
        for (int i = 0; i < circuitBody.Length; i++)
        {
            circuitBody[i].material = enabledTerminal;
        }
    }
}
