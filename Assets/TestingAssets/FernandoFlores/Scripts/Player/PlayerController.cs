﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    // input actions
    // MOVEMENT
    public float moveSpeed;
    float h;
    float v;
    public float angle;
    public float turnSpeed;
    Transform mainCam;
    Quaternion targetRotation;
  //  InputHandler inputs;
    public PlayerModel model;
 //   Vector2 moveDirection;
    public Rigidbody playerRigidbody;
    public Collider playerCollider;
    public Animator zenithAnimator;


    // CONTROL DRONES

    // IS MOVING
    public bool isMoving;
    public bool isSprinting;
    // CROUCH
    public bool isCrouching;

    // INTERACTABLE
    public GameObject interactable;

    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        model = new PlayerModel();
        mainCam = Camera.main.transform;
    }

    // Update is called once per frame
    

    void FixedUpdate()
    {
        if(PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            ApplyAnimations();
            Movement();
            Rotation();
        }
    //    Interact();
    }

    void ApplyAnimations()
    {
        zenithAnimator.SetBool("Moving", isMoving);
        zenithAnimator.SetBool("Crouching", isCrouching);
        zenithAnimator.SetBool("Sprinting", isSprinting);
    }
    
    // PLAYER MOVE
    void Movement()
    {
     //   Vector3 movement = new Vector3(h, 0, v);
        float playerSpeed = CurrentSpeed();
        // TESTING
        Vector3 camF = mainCam.forward;
        Vector3 camR = mainCam.right;
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;
        Vector3 directionRelCamera = camF * v + camR * h;
      //  playerRigidbody.velocity = movement * playerSpeed;
        playerRigidbody.velocity = directionRelCamera * playerSpeed;
        if (playerRigidbody.velocity.magnitude > 0)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
    }

    void Rotation()
    {
        if(h!=0 || v != 0)
        {
            angle = Mathf.Atan2(h, v);
            angle = Mathf.Rad2Deg * angle;
            angle += mainCam.eulerAngles.y;
        }
        targetRotation = Quaternion.Euler(0, angle, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
    }

    float CurrentSpeed()
    {
        float mySpeed;
        if (isSprinting)
        {
            mySpeed = moveSpeed*1.5f;
        }
        else if(isCrouching)
        {
            mySpeed = moveSpeed*0.5f;
        }
        else
        {
            mySpeed = moveSpeed;
        }
        return mySpeed;
    }


    public void MoveDirection(InputAction.CallbackContext context)
    {
     //   if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
     //   {
        Vector2 contextValue = context.ReadValue<Vector2>();
        h = contextValue.x;
        v = contextValue.y;
        // }
    }

    public void Interact(InputAction.CallbackContext context)
    {
        if(PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                if (interactable != null)
                {
                    InteractAction();
                }

            }
        }
    }

    void InteractAction()
    {
        IObjectInteractable tc = interactable.GetComponent<IObjectInteractable>();
        tc.TriggerInteraction();
    }

    public void ActivateEarthBot(InputAction.CallbackContext context)
    {
        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                // SWITCH CAMERA VIEW TO EARTH
                PlayerManager.playerManager.SwitchToGround();
            }

        }
    }

    public void ActivateAirBot(InputAction.CallbackContext context)
    {
        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                // SWITCH CAMERA VIEW TO AIR
                PlayerManager.playerManager.SwitchToAir();
            }

        }
    }

    public void CrouchBehavior(InputAction.CallbackContext context) 
    {
        if(PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            if (context.phase == InputActionPhase.Started)
            {
                Debug.Log("Crouch");
                isCrouching = true;
            }
            else if (context.phase == InputActionPhase.Canceled)
            {
                Debug.Log("StopCrouch");
                isCrouching = false;
            }

        }
        
    }

    public void Sprint(InputAction.CallbackContext context)
    {
        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.Human)
        {
            if (context.phase == InputActionPhase.Started)
            {
                Debug.Log("Sprint");
                isSprinting = true;
            }
            else if (context.phase == InputActionPhase.Canceled)
            {
                Debug.Log("StopSprint");
                isSprinting = false;
            }
        }
    }
    

   
}
