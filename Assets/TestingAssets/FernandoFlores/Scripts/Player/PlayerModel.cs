﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel
{
    private int _hp;
    public int HP {
        get {
            return _hp;
        }
        set {
            _hp = value;
        }
    }

    private int _maxhp;
    public int MaxHP {
        get {
            return _maxhp;
        }
        set {
            _maxhp = value;
        }
    }

    public PlayerModel()
    {
        _hp = _maxhp = 100;
    }
}
