﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AirBotController : MonoBehaviour
{
    public float moveSpeed;
    public float boostSpeed;
    float h;
    float v;
    public float battery;
    public Rigidbody botRigidbody;
    public Collider botCollider;


    public bool hasBeenDeployed;
    public bool hasBootedÚp;

    public bool outOfBattery;
    //public bool consumingBattery; // USANDO...SI
    public bool isThrusting;
    public bool hovering;
    public bool isBoosting;

    float currentForce;
 //   public float hoverValue;
    public float thrustValue;


    public float angle;
    public float turnSpeed;
    Transform mainCam;
    Quaternion targetRotation;
    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasBootedÚp)
        {
         //   AirBotControlSystem();
            BatteryReserves();

            if (battery <= 0 && PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.AirBot)
            {
                outOfBattery = true;
                currentForce = -10;
                ShutDown();
            }
        }
    }

    public void Move(InputAction.CallbackContext context)
    {
            Vector2 contextValue = context.ReadValue<Vector2>();
            h = contextValue.x;
            v = contextValue.y;
    }

    float CurrentSpeed()
    {
        float mySpeed;
        if (!isBoosting)
        {
            mySpeed = moveSpeed;
        }
        else
        {
            mySpeed = boostSpeed;
        }
        return mySpeed;
    }

    private void FixedUpdate()
    {
        if (hasBeenDeployed && hasBootedÚp)
        {
            AirBotControlSystem();
        }
    }

    void AirBotControlSystem()
    {
        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.AirBot)
        {
            Vector3 movement;
            float botSpeed = CurrentSpeed();
            Vector3 camF = mainCam.forward;
            Vector3 camR = mainCam.right;
            camF.y = 0;
            camR.y = 0;
            camF = camF.normalized;
            camR = camR.normalized;
            Vector3 directionRelCamera = camF * v + camR * h;


            if (hovering)
            {
                movement = directionRelCamera;
            //    movement = new Vector3(h, 0, v);
            }
            else
            {
                movement = directionRelCamera + new Vector3(0, currentForce, 0);
            //    movement = new Vector3(h, currentForce, v);
            }

            botRigidbody.velocity = movement * botSpeed;
        }
    }

    void BatteryReserves()
    {
        if (isThrusting)
        {
            battery -= Time.deltaTime * 1.5f;
        }else if (isBoosting)
        {
            battery -= Time.deltaTime * 3.5f;
        }
    }

    public void UseThrusterAxisY(InputAction.CallbackContext context)
    {
        if (!hovering)
        {
            if (context.phase == InputActionPhase.Started)
            {
                currentForce = thrustValue;
                isThrusting = true;
            }
            else if (context.phase == InputActionPhase.Canceled)
            {
                currentForce = 0;
                isThrusting = false;
            }
        }
        
    }

    public void Stabilize(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
        {
            hovering = !hovering;
            botRigidbody.useGravity = !hovering;
        }
    }

    public void TakeControl()
    {
        if (!hasBeenDeployed) // SI NO HA SIDO INICIADO, DEBE HABER UN CD
        {
            transform.SetParent(null);
            botRigidbody.isKinematic = false;
            botRigidbody.AddForce(new Vector3(0, 10f, 0), ForceMode.Impulse);
            StartCoroutine("TurnOnThrusters");
        }
    }


    public void ShutDown()
    {
        //    isActive = false;
        hasBootedÚp = false;
        PlayerManager.playerManager.activeCharacter = PlayerManager.ControlCharacter.Human;
    }

    IEnumerator TurnOnThrusters()
    {
        yield return new WaitForSeconds(0.3f);
        botRigidbody.useGravity = false;
        botRigidbody.velocity = Vector3.zero;
        hasBootedÚp = true;
        hovering = true;
        hasBeenDeployed = true;
    }


    public void ReturnControl(InputAction.CallbackContext context)
    {
        if (PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.AirBot)
        {
            hovering = true;
            botRigidbody.useGravity = false;
            PlayerManager.playerManager.SwitchToHuman();
        //    PlayerManager.playerManager.activeCharacter = PlayerManager.ControlCharacter.Human;
        }
    }

    public void Boost(InputAction.CallbackContext context)
    {
        if (hovering)
        {
            if (context.phase == InputActionPhase.Started)
            {
                isBoosting = true;
            }
            else if (context.phase == InputActionPhase.Canceled)
            {
                Debug.Log("StopSprint");
                isBoosting = false;
            }
        }
       
    }
}
