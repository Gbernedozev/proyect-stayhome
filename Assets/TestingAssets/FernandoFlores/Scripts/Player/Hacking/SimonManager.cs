﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SimonManager : MonoBehaviour
{
    // PARA EL HACK MINIGAME
    public List<SimonButton> myButtons;

    // ORDEN DE INPUTS A REALIZAR
    public List<int> hackSequence;

    // VELOCIDAD DE SHOW
    public float showTime = 0.5f;
    public float pauseTime = 0.5f;

    public int startInputs;
    public int currentInputs;
    
    public int hackInput;

    // NUMERO DE ERRORES
    public int hackMistakes;
    public int maxMistakes;
    
    public bool ai;
    public bool player;

    //    public TerminalController terminalActivator;
    public TerminalControllerAlpha terminalActivator;

    //  public bool hackClear;


    public enum MiniGame
    {
        Standby,
        FailedHack,
        ClearedHack
    }

    public MiniGame success;
    
    
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI scoreText;
    public int score;
    public int clearScore;

    public TextMeshProUGUI explication;
    //   public Button startButton;

    // Start is called before the first frame update
    void Start()
    {
     for(int i = 0; i < myButtons.Capacity; i++)
        {
            myButtons[i].onClick += ButtonClicked;
            myButtons[i].number = i;
        }   
    }

    void ButtonClicked(int _number)
    {
        GetComponentInParent<AudioClipPlayer>().PlayAudio(0,1);//play press button sound
        if (player)
        {
            if(_number == hackSequence[hackInput])
            {
                hackInput++;
            }
            else
            {
                if (hackMistakes >= maxMistakes)
                {
                    FailHack();
                }
                else
                {
                    gameOverText.text = "ERROR";
                    hackMistakes++;
                    BootUp();
                }
            }

            if(hackInput == currentInputs)
            {
                score++;
                scoreText.text = score.ToString();
                currentInputs++;
                hackInput = 0;
                player = false;
                ai = true;
            }

            if(score == clearScore)
            {
                ai = false;
                ClearGame();

                // UNLOCK

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (ai)
        {
            ai = false;
            StartCoroutine("AIFireWall");
        }
    }

    private IEnumerator AIFireWall()
    {
        hackSequence.Clear();

        // INTERACTABLE FALSE
        DisableButtons();
        yield return new WaitForSecondsRealtime(1f);
        for(int i=0;i < currentInputs; i++)
        {
            int myRandom = Random.Range(0, myButtons.Capacity);
            hackSequence.Add(myRandom);
            myButtons[myRandom].ClickedButton();
            yield return new WaitForSecondsRealtime(showTime);
            myButtons[myRandom].UnClickedButton();
            yield return new WaitForSecondsRealtime(showTime);
        }
        player = true;
        gameOverText.text = "INPUT...";
        EnableButtons();
        // INTERACTABLE TRUE
    }

    void DisableButtons()
    {
        for (int i = 0; i < myButtons.Count; i++)
        {
            myButtons[i].gameObject.GetComponent<Button>().interactable = false;
        }
        explication.text = "WAIT...";
    }

    void EnableButtons()
    {
        for (int i = 0; i < myButtons.Count; i++)
        {
            myButtons[i].gameObject.GetComponent<Button>().interactable = true;
        }
        explication.text = "PLEASE REPEAT SEQUENCE...";
    }

    public void StartHack()
    {
        // MOVE TO VIEW
        MiniGameManager.instance.ShowSimonMinigame();
   //     ShowMinigame();
        hackMistakes = 0;
        gameOverText.text = "";
        scoreText.text = score.ToString();
        currentInputs = startInputs;
        score = 0;
        BootUp();
    }

    void BootUp()
    {
        ai = true;
        hackInput = 0;
    //    startButton.interactable = false;
    }

    public void FailHack()
    {
        success = MiniGame.FailedHack;
        gameOverText.text = "GG";
        ai = false;
    //    startButton.interactable = true;
        player = false;
        StartCoroutine("CloseMinigame");
    }

    public void ClearGame()
    {
        success = MiniGame.ClearedHack;
        gameOverText.text = "Clear";
        ai = false;
    //    startButton.interactable = true;
        player = false;
        GetComponentInParent<AudioClipPlayer>().PlayAudio(1,1);//play win sound
        StartCoroutine("CloseMinigame");

    //    startButton.interactable = true;
    //    player = false;
    }

    public void CloseGame()
    {
        success = MiniGame.Standby;
        ai = false;
        player = false;
        Cursor.lockState = CursorLockMode.Locked;
        MiniGameManager.instance.HideSimonMinigame();
        terminalActivator = null;
    }
    

    IEnumerator CloseMinigame()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        Cursor.lockState = CursorLockMode.Locked;
        MiniGameManager.instance.HideSimonMinigame();
        switch (success)
        {
            case MiniGame.ClearedHack:
                terminalActivator.SuccessAction();
                break;
            case MiniGame.FailedHack:
                terminalActivator.FailureAction();
                break;
            default:
                break;
        }
        terminalActivator = null;

    //    HideHackView();
        // TRIGGER CLEAR ACTION
    }

}
