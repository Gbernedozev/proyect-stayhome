﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SimonButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image myImage;

    public Color colorDown;
    public Color colorUp;

    public int number = 99;
    public SimonManager logic;

    public delegate void ClickEv(int number);

    public event ClickEv onClick;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (logic.player)
        {
            ClickedButton();
            onClick.Invoke(number);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        UnClickedButton();
    }

    public void ClickedButton()
    {
        myImage.color = colorDown;
    }

    public void UnClickedButton()
    {
        myImage.color = colorUp;
    }


}
