﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class EarthBotController : MonoBehaviour
{
    public float moveSpeed;
    float h;
    float v;
    public float battery;
    public Rigidbody botRigidbody;
    public Collider botCollider;
    //
    public bool hasBeenDeployed;
    public bool hasBootedÚp;

    public bool outOfBattery;
    //   public bool isActive;


    public float angle;
    public float turnSpeed;
    Transform mainCam;
    Quaternion targetRotation;

    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasBootedÚp)
        {
            if (battery <= 0 && hasBootedÚp)
            {
                outOfBattery = true;
                ShutDown();
            }

        }

    }

    private void FixedUpdate()
    {
        if(hasBeenDeployed && hasBootedÚp)
        {
            Movement();
            Rotation();
        }
    }

    public void Move(InputAction.CallbackContext context)
    {
        Vector2 contextValue = context.ReadValue<Vector2>();
        h = contextValue.x;
        v = contextValue.y;
    }

    void Movement()
    {
        if(PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.EarthBot)
        {
            Vector3 camF = mainCam.forward;
            Vector3 camR = mainCam.right;
            camF.y = 0;
            camR.y = 0;
            camF = camF.normalized;
            camR = camR.normalized;
            Vector3 directionRelCamera = camF * v + camR * h;


        //    Vector3 direction = new Vector3(h, 0, v);
            botRigidbody.velocity = directionRelCamera * moveSpeed;
        }
    }

    void Rotation()
    {
        if (h != 0 || v != 0)
        {
            angle = Mathf.Atan2(h, v);
            angle = Mathf.Rad2Deg * angle;
            angle += mainCam.eulerAngles.y;
        }
        targetRotation = Quaternion.Euler(0, angle, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
    }

    public void TakeControl()
    {
        if (!hasBootedÚp)
        {
            transform.SetParent(null);
            botRigidbody.isKinematic = false;
            StartCoroutine("PrepareToLand");
        }
    }

    public void ShutDown()
    {
        hasBootedÚp = false;
        PlayerManager.playerManager.activeCharacter = PlayerManager.ControlCharacter.Human;
    }

    public void RecoplateToZenith(GameObject player, Transform airInitialPos)
    {
        transform.SetParent(player.transform);
        hasBootedÚp = false;
        botRigidbody.isKinematic = true;
        transform.position = airInitialPos.position;
    }

    IEnumerator PrepareToLand()
    {
        while (true)
        {
            if (PlayerManager.playerManager.IsGrounded(botCollider) == true)
            {
                yield return new WaitForSeconds(0.1f);
                hasBeenDeployed = true;
                hasBootedÚp = true;
                StopCoroutine("PrepareToLand");
            }
            else
            {
                yield return null;
            }
        }
    }


    public void ReturnControl(InputAction.CallbackContext context)
    {
        if(PlayerManager.playerManager.activeCharacter == PlayerManager.ControlCharacter.EarthBot)
        {
            PlayerManager.playerManager.SwitchToHuman();
            //   PlayerManager.playerManager.activeCharacter = PlayerManager.ControlCharacter.Human;
        }
    }
   
}
