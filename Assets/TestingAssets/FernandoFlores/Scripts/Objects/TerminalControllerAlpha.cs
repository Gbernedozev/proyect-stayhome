﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalControllerAlpha : MonoBehaviour, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }

    public enum TypeMinigame
    {
        Simon,
        Claw
    }

    public List<GameObject> panelsToActive;



    public TypeMinigame typeMinigame;

 //   public SimonManager minigame;

    public GameObject[] doorTriggers;
   
    public GameObject lightObj;
    public MeshRenderer terminalMesh;
    public Material lightOn;
    public Material lightOff;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            lightObj.GetComponent<Renderer>().material = lightOn;
            terminalMesh.material = lightOn;
       
        }
        else
        {
            lightObj.GetComponent<Renderer>().material = lightOff;
            terminalMesh.material = lightOff;
        }
        
    }
    
    public void TriggerInteraction()
    {
        if (isActive)
        {
       //     Debug.Log("Activate");
            SelectMinigameToShow();
        }
        else
        {
            Debug.Log("OFFLINE. PLEASE TURN ON POWER");
        }
    }

    public void SuccessAction()
    {
        Debug.Log("UNLOCKING");
        UnlockDoors();
    //    doorTrigger.GetComponent<Sensor>().isActive = true;
    }

    public void UnlockDoors()
    {
        for (int i = 0; i < doorTriggers.Length; i++)
        {
            doorTriggers[i].GetComponent<Sensor>().isActive = true;
        }
    }

    public void FailureAction()
    {
        Debug.Log("You take X damage!!!");
    }

    public void SelectMinigameToShow()
    {
        switch (typeMinigame)
        {
            case TypeMinigame.Simon:
                MiniGameManager.instance.simonController.terminalActivator = this;
                MiniGameManager.instance.simonController.StartHack();
                break;
            default:
                break;
        }
    }


    public void CheckTerminalActivation()
    {
        bool inActive = false;
        for (int i = 0; i < panelsToActive.Count; i++)
        {
            if (panelsToActive[i].GetComponent<PanelController>().isActive)
            {
                inActive = true;
            }
        }
        isActive = inActive;
       
    }
}
