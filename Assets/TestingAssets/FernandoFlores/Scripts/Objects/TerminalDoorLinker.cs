﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalDoorLinker : MonoBehaviour
{
    public GameObject terminal;
    public GameObject door;
    

    public void SelectTerminal(GameObject possibleTerminal)
    {
        if (possibleTerminal.tag == "Terminal")
        {
            terminal = possibleTerminal;
        }
        else
        {
            Debug.LogWarning("ERROR! NOT VALID TERMINAL");
        }
    }

    public void SelectDoor(GameObject possibleDoor)
    {
        if (possibleDoor.tag == "Door")
        {
            door = possibleDoor;
        }
        else
        {
            Debug.LogWarning("ERROR! NOT VALID DOOR");
        }
    }

    public void LinkTerminalToDoor()
    {
        GameObject doorTrigger = door.transform.Find("Activator").gameObject;
    //   terminal.GetComponent<TerminalControllerAlpha>().doorTrigger = doorTrigger;
        Debug.Log("SUCCESS LINKING");
    }




}
