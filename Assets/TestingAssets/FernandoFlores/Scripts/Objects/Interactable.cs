﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectInteractable
{
    bool Interactable { get;set; }
    bool IsActive { get; set; }
    void TriggerInteraction();

}

   