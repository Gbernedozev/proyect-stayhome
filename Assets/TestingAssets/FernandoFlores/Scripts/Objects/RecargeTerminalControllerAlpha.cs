﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecargeTerminalControllerAlpha : MonoBehaviour, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }

  //  public int idCheckpoint;

   // public ElevatorController elevatorAnnexed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void TriggerInteraction()
    {
        if (isActive)
        {
            Debug.Log("RECARGE!!");
        //    LevelManager.levelManager.checkPointIndex = idCheckpoint;
        //    GameStatusChanger.gameStatus.SetNewSpawnPoint(idCheckpoint);
            WeinerController.player.currentHP = WeinerController.player.hp;
            GetComponentInParent<AudioClipPlayer>().PlayAudio(0, 1.1f);
        //    elevatorAnnexed.IsActive = true;
        }
        else
        {
            Debug.Log("ERROR. NO POWER");
        }
        
    }
}
