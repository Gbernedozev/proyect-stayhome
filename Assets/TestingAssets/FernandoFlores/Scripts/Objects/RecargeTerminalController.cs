﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecargeTerminalController : MonoBehaviour, IObjectInteractable
{
    private bool _interactable;
    public bool Interactable {
        get {
            return _interactable;
        }
        set {
            _interactable = value;
        }
    }

    [SerializeField]
    private bool isActive;
    public bool IsActive {
        get {
            return isActive;
        }
        set {
            isActive = value;
        }
    }

    public int idCheckpoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_interactable)
        {
            Debug.Log("Press E to activate Recarging Terminal");
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //    _interactable = true;
            PlayerController pc = other.gameObject.GetComponent<PlayerController>();
            _interactable = true;
            pc.interactable = this.gameObject;
            //   other.gameObject.GetComponent<PlayerController>().interactable = terminal;
            // SHOW PLAYER WHAT CAN HE INTERACT WITH
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerController pc = other.gameObject.GetComponent<PlayerController>();
            _interactable = false;
            pc.interactable = null;
        }
        //   other.gameObject.GetComponent<PlayerController>().interactable = terminal;
        // SHOW PLAYER WHAT CAN HE INTERACT WITH

    }

    public void TriggerInteraction()
    {
        if (_interactable)
        {
            GameManager.gameManager.indexCheckPoint = idCheckpoint;
            Debug.Log("RECHARGE");
        }
    }
}
