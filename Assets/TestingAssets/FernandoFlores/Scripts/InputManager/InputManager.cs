﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;
    InputHandler inputs;
    float isAction;

    public Vector2 inputMovementPlayer;

    private void Awake()
    {
        inputs = new InputHandler();
        inputs.Player.Move.performed += ctx => inputMovementPlayer = ctx.ReadValue<Vector2>();
        inputs.Player.Interact.performed += ctx => isAction = ctx.ReadValue<float>(); 

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    

    public float InteractionValue()
    {
        return isAction;
    }

    // TESTING
    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

  /*  public void UseAction()
    {
        Debug.Log("I'm interacting!");
    }*/

}
