﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwap : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlaySebas()
    {
        SceneManager.LoadScene("SebastianScene", LoadSceneMode.Single);
    }

    public void PlayTavo()
    {
        SceneManager.LoadScene("GustavoScene", LoadSceneMode.Single);
    }
    public void PlayNeos()
    {
        SceneManager.LoadScene("FernandoScene", LoadSceneMode.Single);
    }
}
