﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour
{
     public float RangeTilShutdown;
          public Vector3 PositionSpawned;

     private GameObject MissileTarget;
    
     public float AoE;
     public int Damage;
     private ParticleSystem PS;
     public GameObject explosionPrefab;

     public Transform Target;
     public float RotationSpeed;
 
     private Quaternion _LookRotation;
     private Vector3 _Direction;
	private Rigidbody rb;
	public float speed;
  

 
     void Start () {
         UpdateTarget ();
         PositionSpawned = gameObject.transform.position;
         InvokeRepeating("UpdateTarget",0.5f,0.5f);
         StartCoroutine("EnableCollider",1f);
         PS=GetComponentInChildren<ParticleSystem>();
         rb=GetComponent<Rigidbody>();
         Destroy(gameObject,3);
         GetComponent<Collider>().enabled=false;
         

     }
 
     void Update () {
         if (MissileTarget != null) {
             Target = MissileTarget.transform;
         } 
         if (Vector3.Distance (gameObject.transform.position, PositionSpawned) >= RangeTilShutdown) {
             Destroy (gameObject);
         }

        if(Target!=null){
     
         _Direction = (new Vector3(Target.position.x,Target.position.y+1,Target.position.z)  - transform.position).normalized;
         _LookRotation = Quaternion.LookRotation (_Direction);
		 rb.AddForce(_Direction*speed);
		 
		 


		rb.rotation= Quaternion.Slerp (transform.rotation, _LookRotation, Time.deltaTime * RotationSpeed);
        }
     }
		
	
     

private IEnumerator EnableCollider(){
    yield return new WaitForSeconds(0.1f);
    GetComponent<Collider>().enabled=true;
}
     
  private void OnCollisionEnter(Collision other) {
      Iskill();
     Destroy(gameObject);
     
 }
 private void OnTriggerEnter(Collider other) {
     
             if (other.gameObject.tag == "Player")

        {
          
            other.gameObject.GetComponent<WeinerController>().DamageInflicted(2,false);
           
        }
 }
void DoAoEdamage(){

  
        //Falta hacer raycasting para detectar colisiones
        
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, AoE);
        //int i = 0;
        //while (i < hitColliders.Length)
        {
           //if(hitColliders[i].GetComponent<Player>()!=null){
               //hitColliders[i].GetComponent<Enemy>().life-=Damage;

               
           //}
           //i++;
        }}

        
    

void Iskill(){
 PS.Stop();
 PS.transform.parent = null;
 Destroy(PS.gameObject, 5.0f);
}



     
     	void UpdateTarget ()
	{
		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		
		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

	
		foreach (GameObject player in players)
		{
			
			float distanceToEnemy = Vector2.Distance(new Vector2(transform.position.x,transform.position.z), new Vector2(player.transform.position.x,player.transform.position.z));
			if (distanceToEnemy < shortestDistance)
			{
				shortestDistance = distanceToEnemy;
				nearestEnemy = player;
			}
		}

		if (nearestEnemy != null && shortestDistance <=15)
		{
			Target  = nearestEnemy.transform;
			
		} 
		else
		{
			Target = null;
			 
		}

 
 }

}
