﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour

{
  
  
   
    public float timer;
    

    public float turnSpeed = 210;
    public int life;
    public int damage;
    public Rigidbody rb;
    public int maxlife;
    public GameObject target;
    public Transform navtarget;

    public NavMeshAgent navAgent;
    public bool isChasing;
    public float seekRange;
    public Transform player;
    public bool chaseState;
    public int destPoint;
    public bool alreadyChasing;
    
    public Transform[] points;
    public float WaitTime;
    private Vector3 currenttarget;
 



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (!navAgent.pathPending && navAgent.remainingDistance < 0.5f && !isChasing && !alreadyChasing)
              
            GotoNextPoint();
            

        MoveToTarget();
        if (life <= 0)
        {
          
            Destroy(gameObject);
 
        }
        float dist = Vector3.Distance(transform.position, player.transform.position);
        if (dist < seekRange)
        {
            isChasing = true;
            alreadyChasing=true;
            if(!alreadyChasing)
            InvokeRepeating("SeekTarget", 0.1f, 0.1f);
            
        }
        if (dist > seekRange && isChasing)
        {
            currenttarget = navAgent.destination;

            isChasing = false;
            alreadyChasing=false;
            CancelInvoke("SeekTarget");
            StartCoroutine(LostPlayer());
            //TargetLost();

        }


    }
    public IEnumerator WaitBetweenPoints(){
        navAgent.speed = 0;
        navAgent.velocity = navAgent.velocity*0.7f;
        yield return new WaitForSeconds(WaitTime);
        navAgent.speed = 2f;

    }
    

    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        navAgent.destination = points[destPoint].position;

        destPoint = (destPoint) % points.Length;
    }

    public void MoveToTarget()
    {

        if (navtarget != null)
            navAgent.SetDestination(navtarget.position);
    }
    public void SeekTarget()
    {
       
        Debug.Log("seek");
        
         alreadyChasing=true;
    }
    private void FixedUpdate()
    {
        rb.position = (transform.position + navAgent.velocity * Time.fixedDeltaTime);
        navAgent.nextPosition = rb.position;
    }
    public IEnumerator LostPlayer()
    {
        
        
        Debug.Log("Target Lost");
        navAgent.speed = 0;
        yield return new WaitForSeconds(1.5f);
        navAgent.speed = 2f;
        navtarget = null;
        GotoNextPoint();






    }

   
    public void LookAtPlayer()
    {
        Vector3 targetDir = player.position - transform.position;

        // The step size is equal to speed times frame time.
        float step = turnSpeed * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);


    }
    public void Timer()
    {
        timer -= Time.deltaTime;
    }
}
