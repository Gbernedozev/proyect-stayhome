﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.LightningBolt;
public class Enemy2 : MonoBehaviour {

	public float viewRadius;
	[Range(0,360)]
	public float viewAngle;
	public GameObject bullet;
	public float bulletSpeed;
    float speedMultiplier = 10; /// solo de prueba
	public Transform shotTarget;
	public GameObject lightning;
	public LayerMask targetMask;
	public LayerMask obstacleMask;
	public bool playerSeen;
	public int LookAngle;
	public int LookRotation;
	private float angle;
	public float timeLimit;
	public float timer;
	private float time;
	public float lookSpeed;
	public GameObject[] lights;
	public Transform player;
	private AudioSource ass;
	public AudioClip alert;
	public AudioClip zap;
	
	
public GameObject chaserPrefab;
private CharacterController cc;


	public List<Transform> visibleTargets = new List<Transform>();

	void Start() {
		ass=GetComponent<AudioSource>();
		StartCoroutine ("FindTargetsWithDelay", .2f);
	}
	 private void Update() {
		 
		 if(visibleTargets.Count<=1){
			 foreach(Transform target in visibleTargets){
				shotTarget=target;
				player=target;
				EngageBehaviour();
				Timer();
				lightsRed();
				
			 }
			 



		 }
		 if(visibleTargets.Count==0){
			 Rotate();
			 lightsWhite();
			 timer=0;

				ass.Stop();
			 playerSeen=false;
		 }

	 }
	void Rotate() {
		
		
		if(!playerSeen){
   		angle = Mathf.Sin(time*lookSpeed) * LookAngle;
		   time+=Time.deltaTime;
			transform.rotation = Quaternion.AngleAxis(angle+LookRotation, Vector3.up);
			//transform.rotation = Quaternion.Euler(LookRotation,transform.rotation.y,transform.rotation.z);
		}
}
		 
		 

		
	
	public void EngageBehaviour(){
				if(!playerSeen){
				 playerSeen=true;
			// GameObject bul= Instantiate(bullet,transform.position,Quaternion.identity);
			// Vector3 dir = (shotTarget.transform.position - transform.position).normalized;
			// bul.GetComponent<Rigidbody>().AddForce(dir * bulletSpeed*speedMultiplier); 
			  player.gameObject.GetComponent<WeinerController>().DamageInflicted(1,false);
			 GameObject light= Instantiate(lightning,transform.position,transform.rotation);
			GameObject plpos= player.GetComponent<GameObject>();
			light.GetComponent<LightningBoltScript>().StartObject=gameObject;
			ass.PlayOneShot(zap);
			ass.clip=alert;
			ass.Play();
			light.GetComponent<LightningBoltScript>().EndObject=GameObject.FindGameObjectWithTag("Player");
				}

			 
	}
	


	IEnumerator FindTargetsWithDelay(float delay) {
		while (true) {
			yield return new WaitForSeconds (delay);
			FindVisibleTargets ();
		}
	}

	void FindVisibleTargets() {
		visibleTargets.Clear ();

		Collider[] targetsInViewRadius = Physics.OverlapSphere (transform.position, viewRadius, targetMask);
		for (int i = 0; i < targetsInViewRadius.Length; i++) {
			Transform target = targetsInViewRadius [i].transform;
			Vector3 dirToTarget = (target.position - transform.position).normalized;
			if (Vector3.Angle (transform.forward, dirToTarget) < viewAngle / 2) {
				float dstToTarget = Vector3.Distance (transform.position, target.position);

				if (!Physics.Raycast (transform.position, dirToTarget, dstToTarget, obstacleMask)) {
					visibleTargets.Add (target);
				}
			}
		}
	}
	void lightsWhite(){
	foreach (GameObject light in lights){
		light.GetComponent<Light>().color=Color.white;
	}

	}
	void lightsRed(){
			foreach (GameObject light in lights){
		light.GetComponent<Light>().color=Color.red;
	}


	}
	


	public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
		if (!angleIsGlobal) {
			angleInDegrees += transform.eulerAngles.y;
		}
		return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),0,Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
	}
	void ChasePlayer(){
		Instantiate(chaserPrefab,transform.position,transform.rotation);
		Destroy(gameObject);

	}
	private void Timer(){
		timer+=Time.deltaTime;
		if(timer>=timeLimit){
			ChasePlayer();

		}

	}
}
