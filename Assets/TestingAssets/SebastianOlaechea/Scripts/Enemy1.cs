﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy1 : MonoBehaviour
{
	public enum State{
		Patrolling,
		Chasing,
		Attacking,
		Charging,
	}
	private AudioSource source;
    public AudioClip soundPatrol;
	public GameObject forcefield;
	public Transform playertarget;
	public float chargeTimer;
	public float timeToCharge;
	public float chargeSpeed;
	public int chargeRange;
	public Transform chargePos;
	public Transform navtarget;
	public float normalSpeed;
	public State  state;
	public Transform[] points;
    public List<Transform> visibleTargets = new List<Transform>();
    	public float viewRadius;
	[Range(0,360)]
	public float viewAngle;
	public int destPoint = 0;
    	public LayerMask targetMask;
	public LayerMask obstacleMask;
	private NavMeshAgent navAgent;
    // Start is called before the first frame update
    void Start()
    {
		state=State.Patrolling;
		source=GetComponent<AudioSource>();
		navAgent=GetComponent<NavMeshAgent>();
        StartCoroutine ("FindTargetsWithDelay", .2f);
    }

    // Update is called once per frame
    void Update()
    {
		if(visibleTargets.Count==0){
			playertarget=null;
			state=State.Patrolling;
		}
if(playertarget==null){
if(visibleTargets.Count>0){
		for (int i=0; i< visibleTargets.Count; i++) {
			if (visibleTargets[i].tag == "Player") {
				playertarget=visibleTargets[i];
				state=State.Chasing;


				 
			}		
		}
	}
}

		switch(state){
			default:
			
			case State.Patrolling:
			source.clip=soundPatrol;
			if(!source.isPlaying)
			source.Play(0);
			source.loop=true;
			chargeTimer=0;
			forcefield.SetActive(false);
			navAgent.speed=normalSpeed;
			        if (!navAgent.pathPending && navAgent.remainingDistance < 0.5f){
					GotoNextPoint();
					}

              
            

			break;
			case State.Chasing:
			navAgent.destination=playertarget.transform.position;
			//Debug.Log(Vector3.Distance(transform.position,playertarget.transform.position));
			if(Vector3.Distance(transform.position,playertarget.transform.position)<=chargeRange){
				state=State.Charging;
			}

			break;
			case State.Charging:
			bool hasPlayerPos= false;
			if(!hasPlayerPos){
			chargePos=playertarget.transform;
			hasPlayerPos=true;
			}
			if(hasPlayerPos){
				chargeTimer+=Time.deltaTime;
				navAgent.speed=0;
				if(chargeTimer>=timeToCharge)
				state=State.Attacking;

			}
			

			 break;
			 case State.Attacking:
			 if (!navAgent.pathPending && navAgent.remainingDistance < 0.5f){
					state=State.Patrolling;
					}
			 navAgent.speed=chargeSpeed;
			forcefield.SetActive(true);
	
			 break;
		}
		
			
		
        
    }
    	IEnumerator FindTargetsWithDelay(float delay) {
		while (true) {
			yield return new WaitForSeconds (delay);
			FindVisibleTargets ();
		}
	}
void FollowPlayer(){
navtarget = GameObject.FindGameObjectWithTag("Player").transform;
state=State.Chasing;
}
    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        navAgent.destination = points[destPoint].position;

        destPoint = (destPoint + 1) % points.Length;
    }
	void FindVisibleTargets() {
		visibleTargets.Clear ();

		Collider[] targetsInViewRadius = Physics.OverlapSphere (transform.position, viewRadius, targetMask);
		for (int i = 0; i < targetsInViewRadius.Length; i++) {
			Transform target = targetsInViewRadius [i].transform;
			Vector3 dirToTarget = (target.position - transform.position).normalized;
			if (Vector3.Angle (transform.forward, dirToTarget) < viewAngle / 2) {
				float dstToTarget = Vector3.Distance (transform.position, target.position);

				if (!Physics.Raycast (transform.position, dirToTarget, dstToTarget, obstacleMask)) {
					visibleTargets.Add (target);
				}
			}
		}
	}
    	public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
		if (!angleIsGlobal) {
			angleInDegrees += transform.eulerAngles.y;
		}
		return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),0,Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
	}
}
