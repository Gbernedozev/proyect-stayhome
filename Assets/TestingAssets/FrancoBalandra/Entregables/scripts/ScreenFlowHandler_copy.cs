﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFlowHandler_copy : MonoBehaviour
{
    //DESC: we put this on a gameobject which we want to be an UI element

    string localgamestatus = "playing"; //this is the game´s ui status, as far as the gameobject with this script is aware of...
    public string hud_element = "none"; //the type of ui element we tell the gameobject with this script to be. we input that here
    Vector3 og_scale = new Vector3(0, 0, 0);

    

    // Start is called before the first frame update
    void Start()
    {
        og_scale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
        {
            localgamestatus = GameObject.FindGameObjectWithTag("GlobalUI").GetComponent<GameStatusContainer_copy>().gamestatus; 

                                           //looking for the gameobject with the value of the "gamestatus" variable. in this case, it´s whichever has the tag "GlobalUI", but it can be changed to whatever makes more sense

        switch (localgamestatus)
            {
                case "pause":
                    DrawPauseUI();
                    break;
                case "menu":
                    DrawMenuUI();
                    break;
                case "playing":
                    DrawPlayingUI();
                    break;
                case "death":
                    DrawDeathUI();
                    break;
                default:
                    DrawPlayingUI();
                    break;
            }
        }
       
        void DrawPauseUI()
        {
            if (hud_element == "pause")
            {
                transform.localScale = og_scale;
            }
            else
            {
                transform.localScale = new Vector3(0, 0, 0);
            }
            Cursor.visible = true;
        }
        void DrawMenuUI()
        {
            if (hud_element == "menu")
            {
                transform.localScale = og_scale;
            }
            else
            {
                transform.localScale = new Vector3(0, 0, 0);
            }
            Cursor.visible = true;
        }
        void DrawPlayingUI()
        {
            if (hud_element == "playing")
            { 
                transform.localScale = og_scale;
            }
            else
            {
                transform.localScale = new Vector3(0, 0, 0);
            }
            Cursor.visible = false;
        }
        void DrawDeathUI()
        {
            if (hud_element == "death")
            {
                transform.localScale = og_scale;
            }
            else
            {
                transform.localScale = new Vector3(0, 0, 0);
            }
            Cursor.visible = true;
        }
}
