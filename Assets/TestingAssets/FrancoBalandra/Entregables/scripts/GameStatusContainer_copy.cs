﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatusContainer_copy : MonoBehaviour
{

    //DESC: this decides the global UI status. so there should be only one instance of this script anywhere and everywhere, unless we want things to get complicated
    //NOTE: that means we put this on a gameobject that is never deleted 
    //NOTE 2: you should put the tag that indicates this is the globalUI gameobject
    public string gamestatus = "playing";
 

}
