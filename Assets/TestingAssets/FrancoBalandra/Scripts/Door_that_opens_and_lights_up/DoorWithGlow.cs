﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorWithGlow : MonoBehaviour
{
    GameObject door_plus_center; //door1
    GameObject door_minus_center; //door2
    
    // this thing gets the renderer of the mesh that changes glow color
    MeshRenderer meshrenderer;

    public bool begin_openness = false;//tells the doors to open
    public bool begin_openness_is_only_public_for_debugging = true;//it is

    public float open_duration = 1;  //seconds
    public float open_distance = 2; //relative distance that is
    float currentopenprogress = 0; //current openness progress

    //timer goes like this
    //currentopenprogress =  currentopenprogress + [(deltatime/open_duration)*open_distance]
    // then
    //door.transform.position = door position + (currentopenprogress* relative direction)


    Color lightcolor = new Vector4(0,1,0,1);//(RED,GREEN,BLUE,INTENSITY)




    // Start is called before the first frame update
    void Start()
    {
        door_plus_center = transform.Find("closing_center/door_plus_center").gameObject; //door1
        door_minus_center = transform.Find("closing_center/door_minus_center").gameObject; //door2
    
        
        // this thing gets the renderer of the mesh that changes glow color
        meshrenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
         if (begin_openness == true && currentopenprogress < open_distance)//this thing slides the doors open
        {   currentopenprogress += (Time.deltaTime / open_duration) * open_distance;
          door_minus_center.transform.localPosition = new Vector3(currentopenprogress*-1,0,0);
          door_plus_center.transform.localPosition = new Vector3(currentopenprogress *1, 0, 0);
        }

        //this one´s for debugging
        if (begin_openness == true) { meshrenderer.material.SetColor("_EmissionColor", lightcolor); }
       

    }

    private void OnTriggerEnter(Collider col)// make thing glow and tell doors to slide open
    {
        if(col.tag=="Player"){meshrenderer.material.SetColor("_EmissionColor", Color.blue); begin_openness = true;}
    }
}
