﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipPlayer : MonoBehaviour
{
    public AudioClip[] audio_clip =new AudioClip[1];

     AudioSource audio_player;
    public int current_or_last_audio_index=0;
       
    // Start is called before the first frame update
    void Start()
    {
        audio_player = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAudio(int audio_index,float countdown)//put 0 on countdown to disable countdown and put a negative number to enable looping
    {

        if (audio_index >= 0) { current_or_last_audio_index = audio_index; }
        else { current_or_last_audio_index = 0; }

        audio_player.Stop();
        audio_player.clip = audio_clip[audio_index];
        audio_player.PlayDelayed(0.05f);


        if (countdown < 0) { audio_player.loop = true; countdown = 0; }
        else { audio_player.loop = false; }
        StartCoroutine(autostopintime(countdown));
    }

    IEnumerator autostopintime(float time)
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSecondsRealtime(time);
        if (time > 0)
            {
                audio_player.Stop(); 
            }     
      
       
       
    }

    public void PlayAudioSimple()
    {
        audio_player.Stop();
        audio_player.clip = audio_clip[0];
        audio_player.PlayDelayed(0.05f);
        StartCoroutine(autostopintime(0.6f));
    }
    public void StopAudio(int audio_index)
    {
        if (current_or_last_audio_index == audio_index) { audio_player.Stop(); }
    }
}
