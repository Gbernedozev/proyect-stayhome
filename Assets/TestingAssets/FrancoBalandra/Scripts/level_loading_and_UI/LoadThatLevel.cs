﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadThatLevel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadLevelTutorial()
    {
        SceneManager.LoadSceneAsync(1,LoadSceneMode.Single);
    }
    public void LoadLevelMainMenu()
    {
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
