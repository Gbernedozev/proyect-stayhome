﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFlowHandler : MonoBehaviour
{
    //DESC: we put this on a gameobject which we want to be an UI element or sound source for a given context
    //NOTE: the canvas or sound that holds this has to have the "UI-audio" tag so the ui status changer can control it

   
    public AudioSource[] audiosources;
}
/*
    void Start()
    {
        audiosorces = GetComponents<AudioSource>();

        GetComponent<Canvas>().enabled = false;

        localgamestatus = GameObject.FindGameObjectWithTag("GlobalUI").GetComponent<GameStatusChanger>().global_ui_status;

        switch (localgamestatus)
        {
            case "pause":
                DrawPauseUI();
                break;

            case "menu":
                DrawMenuUI();
                break;

            case "playing":
                DrawPlayingUI();
                Debug.Log("load succesful");
                break;

            case "death":
                DrawDeathUI();
                break;

            case "minigame":
                ShowMinigame();
                break;

            case "gameover":
                DrawGameOverUI();
                break;

            case "debug":
                ShowDebug();
                break;

            default:
                DrawPlayingUI();
                // Debug.Log("NOTHING");
                break;
        }

    }

    void Update()
    {

    }

    public void UpdateUIElementStatus(string status)
    {
        localgamestatus = status;

        switch (localgamestatus)
        {
            case "pause":
                DrawPauseUI();
                break;

            case "menu":
                DrawMenuUI();
                break;

            case "playing":
                DrawPlayingUI();
                Debug.Log("load succesful");
                break;

            case "death":
                DrawDeathUI();
                break;

            case "minigame":
                ShowMinigame();
                break;

            case "gameover":
                DrawGameOverUI();
                break;

            case "debug":
                ShowDebug();
                break;

            default:
                DrawPlayingUI();
                // Debug.Log("NOTHING");
                break;
        }
    }

    void DrawPauseUI()
    {

        if (hud_element == "pause")
        {
            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(true); }//quick fix

            if (GetComponent<Canvas>() != null)
            {
                GetComponent<Canvas>().enabled = true;
            }

            if (GetComponents<AudioSource>() != null)
            {
                foreach (AudioSource item in GetComponents<AudioSource>())
                {
                    GetComponent<AudioSource>().mute = false;
                }
            }
        }
        else
        {
            if (GetComponents<AudioSource>().Length > 1)
            {
                gameObject.SetActive(false);

            }//quick fix


            if (GetComponent<Canvas>() != null)
            {
                GetComponent<Canvas>().enabled = false;
            }
            if (GetComponents<AudioSource>() != null)
            {
                foreach (AudioSource item in GetComponents<AudioSource>())
                {
                    GetComponent<AudioSource>().mute = true;
                }
            }
        }

    }
    void DrawMenuUI()
    {
        if (hud_element == "menu")
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(true); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
            if (GetComponents<AudioSource>() != null)
            {
                foreach (AudioSource item in GetComponents<AudioSource>())
                {
                    GetComponent<AudioSource>().mute = false;
                }
            }
        }
        else
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(false); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
            if (GetComponents<AudioSource>() != null)
            {
                foreach (AudioSource item in GetComponents<AudioSource>())
                {
                    GetComponent<AudioSource>().mute = true;
                }
            }
        }
        // {
        // if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
        //if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = false; }
        //}
        //else
        //{
        // if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
        //    if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = true; }
        //}

    }
    void DrawPlayingUI()
    {
        if (hud_element == "playing")
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(true); }//quick fix


            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
            if (GetComponents<AudioSource>() != null)
            {
                foreach (AudioSource item in GetComponents<AudioSource>())
                {
                    GetComponent<AudioSource>().mute = false;
                }
            }
        }
        else
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(false); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
            if (GetComponents<AudioSource>() != null)
            {
                foreach (AudioSource item in GetComponents<AudioSource>())
                {
                    GetComponent<AudioSource>().mute = true;
                }
            }
        }

    }
    void DrawDeathUI()
    {
        if (hud_element == "death")
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(true); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = false; }
        }
        else
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(false); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = true; }
        }

    }
    void DrawGameOverUI()
    {
        if (hud_element == "gameover")
        {
            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = false; }
        }
        else
        {
            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = true; }
        }

    }

    void ShowMinigame()
    {
        if (hud_element == "minigame")
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(true); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = false; }
        }
        else
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(false); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = true; }
        }
    }

    void ShowDebug()
    {
        if (hud_element == "debug")
        {

            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(true); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = true; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = false; }
        }
        else
        {


            if (GetComponents<AudioSource>().Length > 1) { gameObject.SetActive(false); }//quick fix

            if (GetComponent<Canvas>() != null) { GetComponent<Canvas>().enabled = false; }
            if (GetComponent<AudioSource>() != null) { GetComponent<AudioSource>().mute = true; }

        }
    }
}

    */
//screenflowhandler