﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameStatusChanger : MonoBehaviour

{
    //DESC: this contains the global UI status. so there should be only one instance of this script anywhere and everywhere in the level, unless we want things to get complicated
    //NOTE: this thing is also in charge of changing the global UI status
    //NOTE 2: that means we put this on a gameobject that is never deleted in the level
    //NOTE 3: the gameobject that has it has to have the GlobalUI tag so other scripts can find it and call the functions

    public static GameStatusChanger gameStatus;

    public enum GameState
    {
        Playing,
        Debug,
        Pause,
        Death,
        Minigame
    }

    public GameState gameState;

    public int previousState;

    public GameObject[] ui_element_list;//the array that contains a list of all the ui elements, it is public for debugging purposes

    public GameObject debugWindow;
    public GameObject pauseWindow;
    public GameObject deathWindow;
    public GameObject normalHUD;
    public GameObject minigameWindow;

    public AudioMixerGroup mixerlayer0;
    public AudioMixerGroup mixerlayer1;
    public AudioMixerGroup mixerlayer2;
    public AudioMixerGroup mixerlayer3;

    private void Awake()
    {
        gameStatus = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        ui_element_list = GameObject.FindGameObjectsWithTag("UI-audio");  //ui_element_list gets resized to fit the amount of ui elements in the level + the layer0 player
        StateChange();
        /*
        switch (global_ui_status)
        {
            case "pause":
                PauseAndChangeToPauseUI();
                break;

            case "menu":
                UnpauseAndChangeToMenuUI();
                break;

            case "playing":
                UnpauseAndChangeToPlayingUI();
                break;

            case "death":
                PauseAndChangeToDeathUI();
                break;

            case "minigame":
                EnableMiniGame();
                break;

            case "gameover":
                PauseAndChangeToGameoverUI();
                break;

            case "debug":
                PauseAndDebug();
                break;

            default:
                UnpauseAndChangeToPlayingUI();
                // Debug.Log("NOTHING");
                break;*/
        
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameState == GameState.Pause)
            {
                ResumeGame();
                //    GameStatusChanger.gameStatus.StateChange();
            }
            else
            {
                if(gameState != GameState.Minigame)
                {
                    PauseGame();
                }
                //    GameStatusChanger.gameStatus.StateChange();
            }

            //    GameStatusChanger.gameStatus.StateChange();

            //    ui_status_changer.GetComponent<GameStatusChanger>().PauseAndChangeToPauseUI();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (gameState == GameState.Debug)
            {
                gameStatus.ResumeGame();
            }
            else
            {
                if(gameState != GameState.Minigame)
                {
                    ShowDebugWindow();
                }
            }

            /*    if (ui_status_changer.GetComponent<GameStatusChanger>().global_ui_status == "debug")
                {
                    ui_status_changer.GetComponent<GameStatusChanger>().UnpauseAndChangeToPlayingUI();
                }
                else
                {
                    ui_status_changer.GetComponent<GameStatusChanger>().PauseAndDebug();
                }*/
        }
    }

    public void ResumeGame()
    {
        gameState = (GameState)previousState;
        StateChange();
    }

    public void ResumeNormalPlay()
    {
        gameState = GameState.Playing;
        StateChange();
    }
    

    public void PauseGame()
    {
        previousState = (int)gameState;

        gameState = GameState.Pause;
        StateChange();
    }

    public void DeathScreen()
    {
        gameState = GameState.Death;
        StateChange();
    }

    public void ShowMinigameWindow()
    {
        gameState = GameState.Minigame;
        StateChange();
    }

    public void ShowDebugWindow()
    {
        gameState = GameState.Debug;
        StateChange();
    }


    public void StateChange()
    {
        foreach (GameObject instance in ui_element_list)
        {
            instance.SetActive(false);
        }

        switch (gameState)
        {
            case GameState.Death:
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                GetComponentInChildren<ScreenFader>().ActivateTransition();
                deathWindow.SetActive(true);
                mixerlayer0.audioMixer.SetFloat("layer0volume", -80);
                mixerlayer1.audioMixer.SetFloat("layer1volume", -80);
                mixerlayer2.audioMixer.SetFloat("layer2volume", -80);
                mixerlayer3.audioMixer.SetFloat("layer3volume", -80);
                break;

            case GameState.Debug:
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                debugWindow.SetActive(true);
                mixerlayer0.audioMixer.SetFloat("layer0volume", 0);
                mixerlayer1.audioMixer.SetFloat("layer1volume", 0);
                mixerlayer2.audioMixer.SetFloat("layer2volume", 0);
                mixerlayer3.audioMixer.SetFloat("layer3volume", 0);
                break;

            case GameState.Minigame:
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                minigameWindow.SetActive(true);
                mixerlayer0.audioMixer.SetFloat("layer0volume", -80);//adjust audiolayer0 volume
                mixerlayer1.audioMixer.SetFloat("layer1volume", -80);
                mixerlayer2.audioMixer.SetFloat("layer2volume", 0);
                mixerlayer3.audioMixer.SetFloat("layer3volume", -80);
                break;

            case GameState.Pause:
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                pauseWindow.SetActive(true);
                mixerlayer0.audioMixer.SetFloat("layer0volume", -80);
                mixerlayer1.audioMixer.SetFloat("layer1volume", -80);
                mixerlayer2.audioMixer.SetFloat("layer2volume", 0);
                mixerlayer3.audioMixer.SetFloat("layer3volume", -80);
                break;

            case GameState.Playing:
                Time.timeScale = 1;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                normalHUD.SetActive(true);
                mixerlayer0.audioMixer.SetFloat("layer0volume", 0);
                mixerlayer1.audioMixer.SetFloat("layer1volume", 0);
                mixerlayer2.audioMixer.SetFloat("layer2volume", 0);
                mixerlayer3.audioMixer.SetFloat("layer3volume", 0);
                break;
        }
    }

    

    /*
    public void PauseAndChangeToDeathUI()
    {
        Time.timeScale = 0;
        global_ui_status = "death";
        Cursor.visible = true; Cursor.lockState = CursorLockMode.None;
        GetComponentInChildren<ScreenFader>().ActivateTransition();

        foreach (GameObject instance in ui_element_list)
        { if(instance.GetComponent<ScreenFlowHandler>() != null) {instance.GetComponent<ScreenFlowHandler>().UpdateUIElementStatus(global_ui_status);} }
    }
    
    public void UnpauseAndChangeToPlayingUI()
    {
        Time.timeScale = 1;
        global_ui_status = "playing";
        Cursor.visible = false; Cursor.lockState = CursorLockMode.Locked;//Change cursor behaviour

        foreach (GameObject instance in ui_element_list)
        { if (instance.GetComponent<ScreenFlowHandler>() != null) { instance.GetComponent<ScreenFlowHandler>().UpdateUIElementStatus(global_ui_status); } }

    }
    

    public void PauseAndChangeToPauseUI()
    {
         Time.timeScale = 0;
         global_ui_status = "pause";
        Cursor.visible = true; Cursor.lockState = CursorLockMode.None;

        foreach (GameObject instance in ui_element_list)
        {
            if (instance.GetComponent<ScreenFlowHandler>() != null)
            {
                instance.GetComponent<ScreenFlowHandler>().UpdateUIElementStatus(global_ui_status);
            }
        }

    }

    public void EnableMiniGame()
    {
        Time.timeScale = 0;
        global_ui_status = "minigame";
        Cursor.visible = true; Cursor.lockState = CursorLockMode.None;

        foreach (GameObject instance in ui_element_list)
        { if(instance.GetComponent<ScreenFlowHandler>() != null) {instance.GetComponent<ScreenFlowHandler>().UpdateUIElementStatus(global_ui_status);} }
        
    }

    public void PauseAndDebug()
    {
        Time.timeScale = 0;
        global_ui_status = "debug";
        Cursor.visible = true; Cursor.lockState = CursorLockMode.None;

        foreach (GameObject instance in ui_element_list)
        { if(instance.GetComponent<ScreenFlowHandler>() != null) {instance.GetComponent<ScreenFlowHandler>().UpdateUIElementStatus(global_ui_status);} }

    }

    public void SetNewSpawnPoint(int id)
    {
       // ui_status_container.GetComponent<GameStatusContainer>().spawnIndex = id;//no idea what this does, turning into comment so it doesnt give me an error
    }

    */
}
