﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class PauseGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(GameStatusChanger.gameStatus.gameState == GameStatusChanger.GameState.Pause)
            {
                GameStatusChanger.gameStatus.ResumeGame();
            //    GameStatusChanger.gameStatus.StateChange();
            }
            else
            {
                GameStatusChanger.gameStatus.PauseGame();
            //    GameStatusChanger.gameStatus.StateChange();
            }

        //    GameStatusChanger.gameStatus.StateChange();

        //    ui_status_changer.GetComponent<GameStatusChanger>().PauseAndChangeToPauseUI();
        }

    }

}
