﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemySpawner))]
public class EnemySpawnerEditor : Editor
{
    EnemySpawner spawner;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        spawner = (EnemySpawner)target;
        if(GUILayout.Button("Create Enemy"))
        {
            spawner.CreateEnemy();
        }

    }

}
