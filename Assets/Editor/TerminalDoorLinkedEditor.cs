﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerminalDoorLinker))]
public class TerminalDoorLinkedEditor : Editor
{
    TerminalDoorLinker controller;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        controller = (TerminalDoorLinker)target;
        if (GUILayout.Button("SelectTerminal"))
        {
            controller.SelectTerminal(Selection.activeGameObject);
        }

        if (GUILayout.Button("SelectDoor"))
        {
            controller.SelectDoor(Selection.activeGameObject);
        }

        if (GUILayout.Button("Link"))
        {
            controller.LinkTerminalToDoor();
        }

    }
}
